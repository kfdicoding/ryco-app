package com.f1k375.rycoapp.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.f1k375.rycoapp.data.repositories.auth.AuthRepository
import com.f1k375.rycoapp.domain.UserDomain
import com.f1k375.rycoapp.helpers.OneTimeEvent
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.ui.BaseViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

class AuthViewModel(
    private val repository: AuthRepository,
    private val accountHelper: AccountHelper
) : BaseViewModel() {

    private val _login = MutableLiveData<OneTimeEvent<ViewState<UserDomain>>>()
    val login: LiveData<OneTimeEvent<ViewState<UserDomain>>>
        get() = _login

    fun signIn(email: String, password: String) {
        callRequest(
            liveDataOneTime = _login,
            call = {
                repository.login(email, password)
            },
            success = { data ->
                accountHelper.setLoginData(data)
            }
        )
    }

    private val _register = MutableLiveData<OneTimeEvent<ViewState<Boolean>>>()
    val register: LiveData<OneTimeEvent<ViewState<Boolean>>>
        get() = _register

    fun signUp(name: String, email: String, password: String) {
        callRequest(
            liveDataOneTime = _register,
            call = {
                repository.register(name, email, password)
            }
        )
    }

    companion object {

        fun moduleInstance() = module {
            viewModel { AuthViewModel(get(), get()) }
        }

    }
}
