package com.f1k375.rycoapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.succeeded
import com.f1k375.rycoapp.domain.PaginationDomain
import com.f1k375.rycoapp.helpers.OneTimeEvent
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.toOneTimeEvent
import kotlinx.coroutines.launch

open class BaseViewModel : ViewModel() {

    protected fun <T : Any> callRequest(
        liveData: MutableLiveData<ViewState<T>>? = null,
        liveDataOneTime: MutableLiveData<OneTimeEvent<ViewState<T>>>? = null,
        call: suspend () -> ResultData<T>,
        success: (T) -> Unit = {},
        failed: (message: String) -> Unit = { _ -> }
    ) {
        liveData?.postValue(ViewState.Loading)
        liveDataOneTime?.postValue(ViewState.Loading.toOneTimeEvent())

        viewModelScope.launch {
            val result = call()
            if (result.succeeded) {
                result as ResultData.Success

                success(result.data)

                liveData?.postValue(ViewState.DataLoaded(result.data))
                liveDataOneTime?.postValue(ViewState.DataLoaded(result.data).toOneTimeEvent())
            } else {
                result as ResultData.Failed
                val errorCode = result.errorResponse.errorCode

                val message = StringBuilder()
                message.append(with(result.errorResponse.message) {
                    if (isEmpty()) result.exception?.message ?: "Unknown Error"
                    else "Code : ${result.errorResponse.errorCode}, ${result.errorResponse.message}"
                })

                failed(message.toString())
                liveData?.postValue(ViewState.Error(message.toString(), errorCode))
                liveDataOneTime?.postValue(
                    ViewState.Error(message.toString(), errorCode).toOneTimeEvent()
                )
            }
        }
    }

    protected fun <T : Any> callRequestPagination(
        call: suspend () -> ResultData<PaginationDomain>,
        success: (List<T>, isLast: Boolean) -> Unit = { _, _ -> },
        failed: (message: String) -> Unit = { _ -> }
    ) {
        viewModelScope.launch {
            val result = call()
            if (result.succeeded) {
                result as ResultData.Success
                val data = (result.data.data ?: emptyList<T>()) as List<T>
                success(data, result.data.isLastPage)
            } else {
                result as ResultData.Failed
                val message = StringBuilder()

                message.append(with(result.errorResponse.message) {
                    if (isEmpty())
                        result.exception?.message ?: "Unknown Error"
                    else "Code : ${result.errorResponse.errorCode}, ${result.errorResponse.message}"
                })
                failed(message.toString())
            }
        }
    }

}
