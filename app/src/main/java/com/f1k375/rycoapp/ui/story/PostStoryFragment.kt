package com.f1k375.rycoapp.ui.story

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.FrameLayout
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentPostStoryBinding
import com.f1k375.rycoapp.databinding.LayoutBottomSheetChooseImageBinding
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.dialog.BottomSheetMessageHelper
import com.f1k375.rycoapp.helpers.dialog.LoadingDialogHelper
import com.f1k375.rycoapp.helpers.dialog.MessagesDialogHelper
import com.f1k375.rycoapp.helpers.extension.*
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.utils.FileUtil
import com.f1k375.rycoapp.utils.PermissionUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.io.File
import java.lang.ref.WeakReference

class PostStoryFragment : Fragment(R.layout.fragment_post_story),
    DialogInterface.OnDismissListener {

    private val binding by viewBinding(FragmentPostStoryBinding::bind)
    private val viewModel: StoryViewModel by sharedViewModel()

    private val messageHelper by lazy { MessagesDialogHelper(requireContext()) }
    private val loadingHelper by lazy { LoadingDialogHelper(requireContext()) }
    private val accountHelper: AccountHelper by inject()

    private var imageFile: File? = null
    private lateinit var photoCameraPath: String
    private var isGoBackWhenDismiss = false
    private var permissionsTypeRequest = PERMISSION_STORAGE

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPermissionStorage()
        setView()
        setObserver()

        accountHelper.isLocationOn.doThis {
            binding.switchLocation.isChecked = true
        }
    }

    private fun initPermissionStorage() {
        permissionsTypeRequest = PERMISSION_STORAGE
        when {
            PermissionUtil.isPermissionOpenStorage(requireContext())
                .not() or PermissionUtil.isPermissionOpenCamera(requireContext()).not() -> {
                requestPermissionMultiple.launch(
                    arrayOf(
                        PermissionUtil.permissionOpenStorage(),
                        PermissionUtil.permissionCamera()
                    )
                )
            }
            shouldShowRequestPermissionRationale(PermissionUtil.permissionOpenStorage()) -> {
                messageHelper.showMessageActionToChooseAlert(
                    getString(R.string.title_permission_gallery),
                    getString(R.string.body_permission_gallery),
                    mYesFunction = {
                        requestPermission.launch(PermissionUtil.permissionOpenStorage())
                    }, mNoFunction = {
                        requireActivity().onBackPressed()
                    }
                )
            }
            shouldShowRequestPermissionRationale(PermissionUtil.permissionCamera()) -> {
                messageHelper.showMessageActionToChooseAlert(
                    getString(R.string.title_permission_gallery),
                    getString(R.string.body_permission_gallery),
                    mYesFunction = {
                        requestPermission.launch(PermissionUtil.permissionOpenStorage())
                    }, mNoFunction = {
                        requireActivity().onBackPressed()
                    }
                )
            }
        }
    }

    private fun activeLocation() {
        permissionsTypeRequest = PERMISSION_LOCATION
        if (!PermissionUtil.isPermissionLocationGranted(requireContext())) {
            requestPermission.launch(PermissionUtil.permissionLocation())
        } else if (shouldShowRequestPermissionRationale(PermissionUtil.permissionLocation())) {
            messageHelper.showMessageActionToChooseAlert(
                "Ijin Lokasi Dibutuhkan",
                "Lokasi dibutuhkan untuk mengetahui lokasi anda saat melakukan absensi",
                mYesFunction = {
                    requestPermission.launch(PermissionUtil.permissionLocation())
                }
            )
        } else if (isGpsServiceActive()) {
            viewModel.updateLocation()
        } else {
            binding.switchLocation.isChecked = false
        }

    }

    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
            if (result.not()) {
                showToast("Please Accept Our Request")
                requireActivity().onBackPressed()
            } else {
                when (permissionsTypeRequest) {
                    PERMISSION_STORAGE -> initPermissionStorage()
                    PERMISSION_LOCATION -> activeLocation()
                }
            }
        }

    private val requestPermissionMultiple =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
            if (result.containsValue(false)) {
                showToast("Please Accept Our Request")
                requireActivity().onBackPressed()
            }
        }

    private fun setView() {
        binding.run {
            toolbar.setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }

            switchLocation.setOnCheckedChangeListener { _, isChecked ->
                accountHelper.setLocationActiveOrNote(isChecked)
                isChecked.alsoDoThis {
                    activeLocation()
                }.notDoThis {
                    viewModel.locationPost = null
                }
            }

            editTextDescription.doAfterTextChanged {
                inputTextDescription.isErrorEnabled =
                    (it.toString().trim().length < 10).alsoDoThis {
                        inputTextDescription.error = getString(R.string.role_post_body)
                    }
                updateStateButton()
            }

            imagePost.setOnClickListener {
                chooseActionPickImage()
            }
            buttonChangeImage.setOnClickListener {
                chooseActionPickImage()
            }

            buttonPostStory.setOnClickListener {
                postStory()
            }
        }
    }

    private fun updateStateButton() {
        binding.buttonPostStory.isEnabled =
            imageFile != null && binding.editTextDescription.text.toString().trim().length >= 10
    }

    private fun chooseActionPickImage() {
        val dialog = BottomSheetDialog(requireContext(), R.style.MyBottomSheetDialogTheme)
        val dialogBinding = LayoutBottomSheetChooseImageBinding.inflate(layoutInflater)

        dialogBinding.run {
            imageGallery.setOnClickListener {
                openGallery()
                dialog.dismiss()
            }
            imageCamera.setOnClickListener {
                openCamera()
                dialog.dismiss()
            }
        }

        dialog.setContentView(dialogBinding.root)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setOnShowListener {
            val bottomSheet = it as BottomSheetDialog
            val sheet =
                bottomSheet.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
            sheet?.let { view ->
                BottomSheetBehavior.from(view).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        dialog.show()


    }

    private fun openGallery() {
        val mIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        mIntent.type = "image/*"
        galleryLauncher.launch(mIntent)
    }

    private fun openCamera() {
        val mIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        mIntent.resolveActivity(requireContext().packageManager)
        FileUtil.createTempFile(requireContext()).also {
            val uriPhoto = FileUtil.fileToUri(requireContext(), it)
            photoCameraPath = it.absolutePath
            mIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriPhoto)
            cameraLauncher.launch(mIntent)
        }

    }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                result.data?.data?.let { uri ->
                    displayPhoto(uri)
                }
            }
        }

    private val cameraLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val file = File(photoCameraPath)
            displayPhoto(FileUtil.fileToUri(requireContext(), file))
        }
    }

    private fun displayPhoto(uri: Uri) {
        imageFile = FileUtil.uriToFile(requireContext(), uri)
        Glide.with(requireContext()).load(imageFile?.toUri()).into(binding.imagePost)

        binding.buttonChangeImage.isVisible = true
        updateStateButton()
    }

    private fun postStory() {
        viewModel.addStory(
            WeakReference(requireContext()),
            FileUtil.fileToUri(requireContext(), FileUtil.reduceFileImage(imageFile!!)),
            binding.editTextDescription.text.toString().trim()
        )
    }

    private fun setObserver() {
        postStoryObserver()
        locationObserver()
    }

    private fun postStoryObserver() {
        viewModel.addStory.observe(viewLifecycleOwner) {
            it.consume { result ->
                when (result) {
                    is ViewState.Loading -> loadingHelper.showDialog()
                    is ViewState.Error -> {
                        isGoBackWhenDismiss = false
                        loadingHelper.hideDialog()
                        BottomSheetMessageHelper.showBottomSheetDialogMessage(
                            BottomSheetMessageHelper.FAIL_TASK_MESSAGE,
                            getString(R.string.failed_post_story),
                            result.message,
                            childFragmentManager
                        )
                    }
                    is ViewState.DataLoaded -> {
                        isGoBackWhenDismiss = true
                        loadingHelper.hideDialog()
                        BottomSheetMessageHelper.showBottomSheetDialogMessage(
                            BottomSheetMessageHelper.SUCCESS_TASK_MESSAGE,
                            getString(R.string.success_post_story),
                            getString(R.string.success_post_story_message),
                            childFragmentManager
                        )
                    }
                }
            }
        }
    }

    private fun locationObserver() {
        viewModel.locationUpdate.observe(viewLifecycleOwner) {
            it.consume { result ->
                when (result) {
                    is ViewState.Loading -> loadingHelper.showDialog()
                    is ViewState.DataLoaded -> {
                        loadingHelper.hideDialog()
                    }
                    is ViewState.Error -> {
                        loadingHelper.hideDialog()
                    }
                }
            }
        }
    }

    override fun onDismiss(p0: DialogInterface?) {
        isGoBackWhenDismiss.doThis {
            requireActivity().onBackPressed()
        }
    }

    @SuppressLint("MissingPermission")
    private fun isGpsServiceActive(): Boolean {
        val locationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val gpsStatusEnable =
            locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) ?: false
        val lastLocation = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val isUsingMockLocation = (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            lastLocation?.isMock
        } else {
            lastLocation?.isFromMockProvider
        }) ?: false
        if (!gpsStatusEnable)
            messageHelper.showMessageAlert(
                "GPS Service Disabled",
                "GPS sedang tidak aktif, aktifkan GPS untuk bisa melanjutkan",
                action = {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            )
        else if (isUsingMockLocation) {
            messageHelper.showMessageAlert(
                "Fake GPS Detected",
                "Penggunaan GPS palsu terdeteksi, silakan beralih menggunakan GPS Perangkat."
            )
        }
        return gpsStatusEnable && !isUsingMockLocation
    }


    companion object {
        private const val PERMISSION_STORAGE = 7666
        private const val PERMISSION_LOCATION = 6677
    }
}
