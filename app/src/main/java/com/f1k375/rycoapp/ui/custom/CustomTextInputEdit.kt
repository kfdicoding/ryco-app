package com.f1k375.rycoapp.ui.custom

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.helpers.extension.alsoDoThis
import com.f1k375.rycoapp.utils.RegexUtils
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class CustomTextInputEdit: TextInputEditText, TextWatcher {

    constructor(context: Context): super(context){
        init()
    }
    constructor(context: Context, attrs: AttributeSet): super(context, attrs){
        init()
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr){
        init()
    }

    private fun init(){
        addTextChangedListener(this)
        isSaveEnabled = false
    }


    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun afterTextChanged(editText: Editable?) {
        isSaveEnabled = editText.isNullOrEmpty().not()
        parent?.parent?.let {inputLayout->
            inputLayout as TextInputLayout
            val text = editText.toString().trim()
            val errorTextResource: Int
            val isError = when(inputType){
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_CLASS_TEXT-> {
                    errorTextResource = R.string.email_invalid_message
                    !RegexUtils.isEmailValid(text)
                }
                InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT -> {
                    errorTextResource = R.string.password_invalid_message
                    !RegexUtils.isPasswordValid(text)
                }
                else -> {
                    errorTextResource = 0
                    false
                }
            }
            inputLayout.run {
                isErrorEnabled = isError.alsoDoThis {
                    error = rootView.context.getString(errorTextResource)
                }
            }
        }
    }
}
