package com.f1k375.rycoapp.ui.story

import android.content.Context
import android.location.Location
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.f1k375.rycoapp.data.models.request.story.StoryBody
import com.f1k375.rycoapp.data.remote.createMultiPartBody
import com.f1k375.rycoapp.data.repositories.story.StoryRepository
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.domain.asDomain
import com.f1k375.rycoapp.helpers.OneTimeEvent
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.location.GeoLocationDataSource
import com.f1k375.rycoapp.helpers.toOneTimeEvent
import com.f1k375.rycoapp.ui.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.lang.ref.WeakReference

class StoryViewModel(
    private val repository: StoryRepository,
    private val locationDataSource: GeoLocationDataSource
) : BaseViewModel() {

    private val _storiesLocation = MutableLiveData<OneTimeEvent<ViewState<List<StoryDomain>>>>()
    val storiesLocationObserver: LiveData<OneTimeEvent<ViewState<List<StoryDomain>>>>
        get() = _storiesLocation

    val storiesLocationData = ArrayList<StoryDomain>()
    fun getPostStoryLocation() {
        callRequest(
            liveDataOneTime = _storiesLocation,
            call = {
                _storiesLocation.postValue(ViewState.Loading.toOneTimeEvent())
                repository.getStoriesLocation()
            },
            success = { data ->
                storiesLocationData.clear()
                storiesLocationData.addAll(data)
            }
        )
    }

    var keySearch = ""
    fun searchStories(key: String) {
        keySearch = key
        filterKeySearch.tryEmit(keySearch)
    }

    private val filterKeySearch = MutableStateFlow("")

    @OptIn(ExperimentalCoroutinesApi::class)
    val pagingStory: Flow<PagingData<StoryDomain>>
        get() = filterKeySearch.flatMapLatest { keySearch ->
            System.out.println("key search $keySearch")
            repository.getStories(keySearch)
        }.map {
            it.map { story -> story.asDomain }
        }.cachedIn(viewModelScope)

    private val _addStory = MutableLiveData<OneTimeEvent<ViewState<Boolean>>>()
    val addStory: LiveData<OneTimeEvent<ViewState<Boolean>>>
        get() = _addStory

    fun addStory(context: WeakReference<Context>, uri: Uri?, desc: String) {
        callRequest(
            liveDataOneTime = _addStory,
            call = {
                repository.addStory(
                    StoryBody(
                        photo = context.get()?.let { createMultiPartBody(it, uri, "photo") },
                        description = desc,
                        lon = locationPost?.longitude,
                        lat = locationPost?.latitude
                    )
                )
            }, success = {
                searchStories(keySearch)
                locationPost = null
            }
        )
    }

    var locationPost: Location? = null
    private val _locationUpdate = MutableLiveData<OneTimeEvent<ViewState<Location>>>()
    val locationUpdate: LiveData<OneTimeEvent<ViewState<Location>>>
        get() = _locationUpdate

    fun updateLocation() {
        viewModelScope.launch {
            _locationUpdate.postValue(ViewState.Loading.toOneTimeEvent())
            locationDataSource.locationObservable.collectLatest {
                _locationUpdate.postValue(ViewState.DataLoaded(it).toOneTimeEvent())
                locationPost = it
                this.cancel()
            }
        }
    }


    companion object {
        fun moduleInstance() = module {
            viewModel { StoryViewModel(get(), get()) }
        }
    }
}
