package com.f1k375.rycoapp.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentSignInBinding
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.dialog.BottomSheetMessageHelper
import com.f1k375.rycoapp.helpers.dialog.LoadingDialogHelper
import com.f1k375.rycoapp.helpers.extension.doThis
import com.f1k375.rycoapp.helpers.extension.viewBinding
import com.f1k375.rycoapp.ui.story.HomeStoryActivity
import com.f1k375.rycoapp.utils.AnimationUtils
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignInFragment: Fragment(R.layout.fragment_sign_in) {

    private val binding by viewBinding(FragmentSignInBinding::bind)
    private val viewModel: AuthViewModel by viewModel()

    private val loadingHelper by lazy { LoadingDialogHelper(requireContext())}

    private val isFormValid: Boolean
    get(){
        binding.editTextEmail.text.toString().isEmpty().doThis {
            binding.editTextEmail.setText("")
        }

        binding.editTextPassword.text.toString().isEmpty().doThis {
            binding.editTextPassword.setText("")
        }
        return !(binding.inputLayoutEmail.isErrorEnabled || binding.inputLayoutPassword.isErrorEnabled)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpView()
        setAnimation()
        setObserver()
    }

    private fun setUpView(){
        binding.run{
            buttonLogin.setOnClickListener{
                isFormValid.doThis{
                    viewModel.signIn(
                        editTextEmail.text.toString(),
                        editTextPassword.text.toString()
                    )
                }
            }

            buttonRegister.setOnClickListener{
                findNavController().navigate(
                    SignInFragmentDirections.actionSignInFragmentToSignUpFragment()
                )
            }
        }
    }

    private fun setAnimation(){
        binding.root.post{
            AnimationUtils.slideInLeftToRight(binding.textHeading).start()
            AnimationUtils.slideInRightToLeft(binding.imageIllustration).start()
            AnimationUtils.slideInBottomToTop(binding.inputLayoutEmail).start()
            AnimationUtils.slideInBottomToTop(binding.inputLayoutPassword).start()
            AnimationUtils.fadeIn(binding.buttonLogin).start()
            AnimationUtils.fadeIn(binding.buttonRegister).start()
        }
    }

    private fun setObserver(){
        viewModel.login.observe(viewLifecycleOwner){it.consume { result ->
            when(result){
                is ViewState.Loading -> loadingHelper.showDialog()
                is ViewState.Error ->{
                    loadingHelper.hideDialog()
                    BottomSheetMessageHelper.showBottomSheetDialogMessage(
                        type = BottomSheetMessageHelper.FAIL_TASK_MESSAGE,
                        title = getString(R.string.text_title_failed_login),
                        desc = result.message,
                        fm = childFragmentManager
                    )
                }
                is ViewState.DataLoaded -> {
                    loadingHelper.hideDialog()
                    startActivity(Intent(requireContext(), HomeStoryActivity::class.java))
                    requireActivity().finish()
                }
            }
        }}
    }

}
