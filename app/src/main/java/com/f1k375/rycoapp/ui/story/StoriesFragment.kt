package com.f1k375.rycoapp.ui.story

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentStoriesBinding
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.domain.UserDomain
import com.f1k375.rycoapp.helpers.adapter.AdapterListener
import com.f1k375.rycoapp.helpers.adapter.LoadStateAdapter
import com.f1k375.rycoapp.helpers.dialog.MessagesDialogHelper
import com.f1k375.rycoapp.helpers.extension.*
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.ui.auth.AuthenticationActivity
import kotlinx.coroutines.flow.collectLatest
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class StoriesFragment : Fragment(R.layout.fragment_stories) {

    private val binding by viewBinding(FragmentStoriesBinding::bind)
    private val viewModel: StoryViewModel by sharedViewModel()

    private val accountHelper: AccountHelper by inject()
    private val messageHelper by lazy { MessagesDialogHelper(requireContext()) }
    private val adapterStories by lazy { StoriesAdapterPaging() }

    private val userData: UserDomain?
        get() = accountHelper.userData

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setView()
        setObserver()
        initData()
    }

    private fun initData() {
        binding.editTextSearch.setText(viewModel.keySearch)
    }

    private fun setView() {
        setHeaderSection()
        setAdapter()
        binding.run {
            buttonAddStory.setOnClickListener {
                findNavController().navigate(
                    StoriesFragmentDirections.actionStoriesFragmentToPostStoryFragment()
                )
            }
            buttonMapStory.setOnClickListener {
                findNavController().navigate(
                    StoriesFragmentDirections.actionStoriesFragmentToStoriesLocation()
                )
            }
            swipeRefresh.setOnRefreshListener {
                if (swipeRefresh.isRefreshing) {
                    adapterStories.refresh()
                }
            }
        }
    }

    private fun setHeaderSection() {
        userData?.name?.let {
            binding.bgImageProfile.backgroundTintList =
                ColorStateList.valueOf(Color.parseColor(it.toColorHex()))
            binding.textInitialName.text = it.asNameInitial()
        }

        binding.editTextSearch.onTextChangesSearchTypePurpose(lifecycleScope, delay = 700) { key ->
            viewModel.searchStories(key.toString())
        }

        binding.buttonLogout.setOnClickListener {
            messageHelper.showMessageActionToChooseAlert(
                getString(R.string.logout),
                getString(R.string.logout_message),
                isDangerAction = true,
                mYesFunction = {
                    accountHelper.logout()
                    startActivity(Intent(requireContext(), AuthenticationActivity::class.java))
                    requireActivity().finish()
                }
            )
        }
    }

    private fun setAdapter() {

        adapterStories.addOnItemClickListener(object : AdapterListener.OnClickItem {
            override fun onClickItem(data: Any?, position: Int, view: View?) {
                data as StoryDomain
                view?.let {
                    val extraTransition =
                        FragmentNavigator.Extras.Builder().addSharedElement(it, data.id)
                            .build()
                    findNavController().navigate(
                        StoriesFragmentDirections.actionStoriesFragmentToStoryDetailFragment(data),
                        extraTransition
                    )
                }
            }

        })

        adapterStories.addLoadStateListener { loadState ->
            if (loadState.refresh !is LoadState.Loading) {
                binding.swipeRefresh.isRefreshing = false
            }

            setEmptyStateLayout(loadState.source.refresh, loadState.append)
            setInitialStateLayout(loadState.source.refresh)
        }
        binding.rvPostStory.adapter = adapterStories.withLoadStateFooter(
            footer = LoadStateAdapter { adapterStories.retry() },
        )
    }

    private fun setEmptyStateLayout(state: LoadState, append: LoadState) {
        binding.layoutEmpty.root.isVisible =
            (state is LoadState.NotLoading && append.endOfPaginationReached && adapterStories.itemCount == 0)
    }

    private fun setInitialStateLayout(state: LoadState) {
        binding.layoutStateInitial.root.isVisible =
            ((state is LoadState.Loading || state is LoadState.Error) && adapterStories.itemCount == 0).alsoDoThis {
                binding.layoutStateInitial.run {
                    loadingLayout.root.isVisible = state is LoadState.Loading
                    errorLayout.root.isVisible = (state is LoadState.Error).alsoNotDoThis {
                        errorLayout.buttonRetry.setOnClickListener {
                            adapterStories.retry()
                        }
                    }
                }
            }
    }

    private fun setObserver() {
        lifecycleScope.launchWhenResumed {
            viewModel.pagingStory.collectLatest { pagingData ->
                adapterStories.submitData(lifecycle, pagingData)
            }
        }
    }
}
