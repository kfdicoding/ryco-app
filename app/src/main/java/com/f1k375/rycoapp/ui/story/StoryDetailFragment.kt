package com.f1k375.rycoapp.ui.story

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.transition.ChangeImageTransform
import android.transition.ChangeTransform
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentStoryDetailBinding
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.helpers.extension.asNameInitial
import com.f1k375.rycoapp.helpers.extension.toColorHex

class StoryDetailFragment: Fragment(R.layout.fragment_story_detail) {

    private lateinit var binding : FragmentStoryDetailBinding

    private val args by navArgs<StoryDetailFragmentArgs>()
    private val storyItem: StoryDomain? by lazy { args.story }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        transitionSetting()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStoryDetailBinding.inflate(layoutInflater, container, false)
        storyItem?.id?.let { binding.imagePost.transitionName = it }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setView()
    }

    private fun transitionSetting(){
        sharedElementEnterTransition = ChangeTransform()
        sharedElementReturnTransition = ChangeImageTransform()
    }

    private fun setView(){
        setToolbar()
        storyItem?.let {
            binding.run{

                textInitialName.text = it.name.asNameInitial()
                bgImageProfile.backgroundTintList = ColorStateList.valueOf(Color.parseColor(it.name.toColorHex()))
                textName.text = it.name
                textDatePost.text = it.postTimeFull()
                textBodyPost.text = it.description

                Glide.with(requireContext()).load(it.photo).into(imagePost)
            }
        }
    }

    private fun setToolbar(){
        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
    }

}
