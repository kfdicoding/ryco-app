package com.f1k375.rycoapp.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.ui.story.HomeStoryActivity
import org.koin.android.ext.android.inject

class AuthenticationActivity : AppCompatActivity() {

    private val accountHelper: AccountHelper by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        if (accountHelper.isLogin){
            startActivity(Intent(this, HomeStoryActivity::class.java))
            finish()
        }
    }
}
