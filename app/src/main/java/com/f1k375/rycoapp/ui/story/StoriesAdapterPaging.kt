package com.f1k375.rycoapp.ui.story

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.f1k375.rycoapp.databinding.ItemStoryPostBinding
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.helpers.adapter.AdapterListener
import com.f1k375.rycoapp.helpers.extension.asNameInitial
import com.f1k375.rycoapp.helpers.extension.toColorHex
import java.lang.ref.WeakReference

class StoriesAdapterPaging :
    PagingDataAdapter<StoryDomain, StoriesAdapterPaging.ViewHolderStory>(DIFF_CALLBACK) {

    inner class ViewHolderStory(private val mBinding: ItemStoryPostBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        fun bind(item: StoryDomain) {
            mBinding.run {
                imagePost.transitionName = item.id

                textInitialName.text = item.name.asNameInitial()
                bgImageProfile.backgroundTintList =
                    ColorStateList.valueOf(Color.parseColor(item.name.toColorHex()))
                textName.text = item.name
                textDatePost.text = item.postTime(WeakReference(root.context))
                textBodyPost.text = item.description

                Glide.with(root.context).load(item.photo).into(imagePost)

                root.setOnClickListener {
                    mClickListener?.onClickItem(item, 0, imagePost)
                }
            }
        }

        fun clearResource() {
            Glide.with(mBinding.root.context).clear(mBinding.imagePost)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<StoryDomain>() {
            override fun areItemsTheSame(oldItem: StoryDomain, newItem: StoryDomain): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: StoryDomain, newItem: StoryDomain): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolderStory, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderStory {
        return ViewHolderStory(
            ItemStoryPostBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }


    private var mClickListener: AdapterListener.OnClickItem? = null
    fun addOnItemClickListener(listener: AdapterListener.OnClickItem) {
        mClickListener = listener
    }

    override fun onViewRecycled(holder: ViewHolderStory) {
        super.onViewRecycled(holder)
        holder.clearResource()
    }
}
