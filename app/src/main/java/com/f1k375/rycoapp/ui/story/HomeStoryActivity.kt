package com.f1k375.rycoapp.ui.story

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.f1k375.rycoapp.R

class HomeStoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_story)
    }
}
