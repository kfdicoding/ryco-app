package com.f1k375.rycoapp.ui.story

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentStoriesLocationBinding
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.dialog.BottomSheetMessageHelper
import com.f1k375.rycoapp.helpers.dialog.LoadingDialogHelper
import com.f1k375.rycoapp.helpers.extension.doThis
import com.f1k375.rycoapp.helpers.extension.notDoThis
import com.f1k375.rycoapp.helpers.extension.showToast
import com.f1k375.rycoapp.helpers.extension.viewBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import org.koin.androidx.viewmodel.ext.android.viewModel

class StoriesLocationFragment : Fragment(R.layout.fragment_stories_location), OnMapReadyCallback,
    DialogInterface.OnDismissListener {

    private val binding by viewBinding(FragmentStoriesLocationBinding::bind)
    private val viewModel: StoryViewModel by viewModel()

    private val loadingHelper by lazy { LoadingDialogHelper(requireContext()) }

    private lateinit var googleMap: GoogleMap
    private var isGoBackWhenDismiss = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mapView.getFragment<SupportMapFragment?>()?.getMapAsync(this)

        initLoadData()
        setObserver()
    }

    private fun initLoadData() {
        viewModel.getPostStoryLocation()
    }

    private fun setObserver() {
        viewModel.storiesLocationObserver.observe(viewLifecycleOwner) {
            it.consume { result ->
                when (result) {
                    is ViewState.Loading -> loadingHelper.showDialog()
                    is ViewState.Error -> {
                        isGoBackWhenDismiss = true
                        loadingHelper.hideDialog()
                        BottomSheetMessageHelper.showBottomSheetDialogMessage(
                            BottomSheetMessageHelper.FAIL_TASK_MESSAGE,
                            getString(R.string.failed_get_stories_location_message),
                            getString(R.string.failed_get_stories_location_message),
                            childFragmentManager
                        )
                    }
                    is ViewState.DataLoaded -> {
                        isGoBackWhenDismiss = false
                        loadingHelper.hideDialog()
                        setMapMarkers()
                    }
                }
            }
        }
    }

    private var isMarkerSet = false
    private fun setMapMarkers() {
        (this::googleMap.isInitialized && !isMarkerSet).doThis {
            isMarkerSet = true

            val locations = ArrayList<LatLng>()
            viewModel.storiesLocationData.forEach { story ->
                val location = LatLng(story.latitude, story.longitude)
                googleMap.addMarker(
                    MarkerOptions().position(location).title(story.name)
                        .snippet(story.description.take(20))
                )
                locations.add(location)
            }

            viewModel.storiesLocationData.isNotEmpty().doThis {
                googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(locations.random(), 5f)
                )
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map.apply {
            uiSettings.isCompassEnabled = true
            uiSettings.isZoomControlsEnabled = true
        }
        setMapMarkers()

        try {
            val isLoaded = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    requireContext(), R.raw.map_style
                )
            )
            isLoaded.notDoThis {
                showToast(getString(R.string.failed_load_map_style))
            }
        } catch (error: Exception) {
            showToast(getString(R.string.failed_load_map_style))
        }
    }

    override fun onDismiss(p0: DialogInterface?) {
        isGoBackWhenDismiss.doThis {
            requireActivity().onBackPressed()
        }
    }

}
