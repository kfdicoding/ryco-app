package com.f1k375.rycoapp.ui.auth

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.FragmentSignUpBinding
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.dialog.BottomSheetMessageHelper
import com.f1k375.rycoapp.helpers.dialog.LoadingDialogHelper
import com.f1k375.rycoapp.helpers.extension.doThis
import com.f1k375.rycoapp.helpers.extension.viewBinding
import com.f1k375.rycoapp.utils.AnimationUtils
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment: Fragment(R.layout.fragment_sign_up), DialogInterface.OnDismissListener {

    private val binding by viewBinding(FragmentSignUpBinding::bind)
    private val viewModel: AuthViewModel by viewModel()

    private val loadingHelper by lazy { LoadingDialogHelper(requireContext()) }

    private val isFormValid: Boolean
        get(){
            binding.editTextName.text.toString().isEmpty().doThis{
                binding.inputLayoutName.isErrorEnabled = true
                binding.inputLayoutName.error = getString(R.string.please_fill_the_field)
            }
            binding.editTextEmail.text.toString().isEmpty().doThis {
                binding.editTextEmail.setText("")
            }

            binding.editTextPassword.text.toString().isEmpty().doThis {
                binding.editTextPassword.setText("")
            }

            return !(binding.inputLayoutName.isErrorEnabled || binding.inputLayoutEmail.isErrorEnabled || binding.inputLayoutPassword.isErrorEnabled)
        }

    private var isBackWhenSheetDismiss = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setView()
        setAnimation()
        setObserver()
    }

    private fun setView() {
        binding.run{
            editTextName.doAfterTextChanged {
                inputLayoutName.isErrorEnabled = false
            }

            buttonRegister.setOnClickListener{
                isFormValid.doThis{
                    viewModel.signUp(
                        editTextName.text.toString(),
                        editTextEmail.text.toString(),
                        editTextPassword.text.toString()
                    )
                }
            }
        }
    }

    private fun setAnimation(){
        binding.root.post{
            AnimationUtils.slideInLeftToRight(binding.textHeading).start()
            AnimationUtils.slideInRightToLeft(binding.imageIllustration).start()
            AnimationUtils.slideInBottomToTop(binding.inputLayoutName).start()
            AnimationUtils.slideInBottomToTop(binding.inputLayoutEmail).start()
            AnimationUtils.slideInBottomToTop(binding.inputLayoutPassword).start()
            AnimationUtils.fadeIn(binding.buttonRegister).start()
        }
    }

    private fun setObserver() {
        viewModel.register.observe(viewLifecycleOwner){it.consume { result->
            when (result){
                is ViewState.Loading -> loadingHelper.showDialog()
                is ViewState.Error -> {
                    loadingHelper.hideDialog()
                    isBackWhenSheetDismiss = false
                    BottomSheetMessageHelper.showBottomSheetDialogMessage(
                        BottomSheetMessageHelper.FAIL_TASK_MESSAGE,
                        getString(R.string.text_title_failed_sign_up),
                        result.message,
                        childFragmentManager
                    )
                }
                is ViewState.DataLoaded -> {
                    loadingHelper.hideDialog()
                    isBackWhenSheetDismiss = true
                    BottomSheetMessageHelper.showBottomSheetDialogMessage(
                        BottomSheetMessageHelper.SUCCESS_TASK_MESSAGE,
                        getString(R.string.text_title_success_sign_up),
                        getString(R.string.text_body_success_sign_up),
                        childFragmentManager
                    )
                }
            }
        }}
    }

    override fun onDismiss(p0: DialogInterface?) {
        isBackWhenSheetDismiss.doThis {
            requireActivity().onBackPressed()
        }
    }

}
