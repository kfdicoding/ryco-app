package com.f1k375.rycoapp.data.models

import com.google.gson.annotations.SerializedName

open class BaseResponse<T> {
    @SerializedName("error")
    var isError: Boolean = false
    var message: String? = null
    var errorCode: Int = 0
    open val data: T? = null
    val currentPage: Int? = null
    val lastPage: Int? = null
    val total: Int? = null
}
