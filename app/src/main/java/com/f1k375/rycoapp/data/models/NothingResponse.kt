package com.f1k375.rycoapp.data.models

import com.google.gson.annotations.SerializedName

data class NothingResponse(@SerializedName("nothing") override val data: Nothing?): BaseResponse<Nothing>()
