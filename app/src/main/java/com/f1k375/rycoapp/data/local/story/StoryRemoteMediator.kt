package com.f1k375.rycoapp.data.local.story

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.f1k375.rycoapp.data.local.LocalClient
import com.f1k375.rycoapp.data.local.remotekey.RemoteKey
import com.f1k375.rycoapp.data.local.story.PagingHelper.INITIAL_PAGE
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.story.StoryPersistence
import com.f1k375.rycoapp.data.remote.succeeded
import com.f1k375.rycoapp.domain.PaginationDomain
import com.f1k375.rycoapp.domain.asPaginationDomain

object PagingHelper {
    var INITIAL_PAGE = 1
}

@OptIn(ExperimentalPagingApi::class)
class StoryRemoteMediator(
    private val db: LocalClient,
    private val persistence: StoryPersistence,
    private val keySearch: String,
) : RemoteMediator<Int, StoryEntity>() {
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, StoryEntity>
    ): MediatorResult {
        var page = getPage(loadType, state)
        if (page == 0) return MediatorResult.Success(endOfPaginationReached = false)

        return try {

            var data: List<StoryEntity>
            var isLastPage: Boolean
            var result: PaginationDomain

            var response = persistence.stories(page, state.config.pageSize)

            if (response.succeeded) {
                response as ResultData.Success
                result = response.data.asPaginationDomain()
                isLastPage = result.isLastPage
                data = getFilteredData(response.data.data ?: emptyList())

                while (data.isEmpty() && !isLastPage) {

                    response = persistence.stories(++page, state.config.pageSize)
                    if (response.succeeded) {
                        response as ResultData.Success
                        result = response.data.asPaginationDomain()
                        isLastPage = result.isLastPage
                        data = getFilteredData(response.data.data ?: emptyList())
                    } else {
                        response as ResultData.Failed
                        return MediatorResult.Error(
                            response.exception
                                ?: Exception("Error happen ${response.errorResponse.errorCode} ${response.errorResponse.message}")
                        )
                    }
                }

                db.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        INITIAL_PAGE = if (isLastPage) 1 else page
                        db.remoteKeyDao.deleteAll()
                        db.storyDao.deleteAll()
                    }

                    val prevKey = if (page == INITIAL_PAGE) null else page - 1
                    val nextKey = if (isLastPage) null else page + 1
                    val keys = data.map {
                        RemoteKey(it.id, prevKey, nextKey)
                    }

                    db.remoteKeyDao.addKey(*keys.toTypedArray())
                    db.storyDao.addStory(*data.toTypedArray())
                }
                MediatorResult.Success(endOfPaginationReached = isLastPage)
            } else {
                response as ResultData.Failed
                MediatorResult.Error(
                    response.exception
                        ?: Exception("Error happen ${response.errorResponse.errorCode} ${response.errorResponse.message}")
                )
            }
        } catch (exception: Exception) {
            MediatorResult.Error(exception)
        }
    }

    private suspend fun getPage(loadType: LoadType, state: PagingState<Int, StoryEntity>): Int {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: INITIAL_PAGE
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                val prevKey = remoteKeys?.prevKey
                    ?: 0
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                val nextKey = remoteKeys?.nextKey
                    ?: 0
                nextKey
            }
        }
    }

    private fun getFilteredData(data: List<StoryResponse>): List<StoryEntity> {
        return data.map { it.asEntity }.toList().filter {
            it.name.lowercase().contains(keySearch) || it.description.lowercase()
                .contains(keySearch)
        }
    }

    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, StoryEntity>): RemoteKey? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()?.let { data ->
            db.remoteKeyDao.getKey(data.id)
        }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, StoryEntity>): RemoteKey? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()?.let { data ->
            db.remoteKeyDao.getKey(data.id)
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, StoryEntity>): RemoteKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                db.remoteKeyDao.getKey(id)
            }
        }
    }
}