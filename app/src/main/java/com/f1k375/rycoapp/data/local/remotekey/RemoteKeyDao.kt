package com.f1k375.rycoapp.data.local.remotekey

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RemoteKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addKey(vararg remoteKey: RemoteKey)

    @Query("SELECT * FROM remote_keys WHERE id = :id")
    suspend fun getKey(id: String): RemoteKey?

    @Query("DELETE FROM remote_keys")
    suspend fun deleteAll()
}