package com.f1k375.rycoapp.data.local.story

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse

@Entity(tableName = "stories")
data class StoryEntity(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val name: String,
    val description: String,
    val photo: String,
    val createdAt: String,
    val latitude: Double,
    val longitude: Double,
)

val StoryResponse.asEntity: StoryEntity
    get() = StoryEntity(
        id = id ?: "",
        name = name ?: "",
        description = description ?: "",
        photo = photoUrl ?: "",
        createdAt = createdAt ?: "",
        latitude = lat ?: 0.0,
        longitude = lon ?: 0.0
    )
