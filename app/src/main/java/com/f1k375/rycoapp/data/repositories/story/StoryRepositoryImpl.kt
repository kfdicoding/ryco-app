package com.f1k375.rycoapp.data.repositories.story

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.f1k375.rycoapp.data.local.LocalClient
import com.f1k375.rycoapp.data.local.story.StoryEntity
import com.f1k375.rycoapp.data.local.story.StoryRemoteMediator
import com.f1k375.rycoapp.data.models.request.story.StoryBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.story.StoryPersistence
import com.f1k375.rycoapp.data.remote.succeeded
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.domain.asDomain
import kotlinx.coroutines.flow.Flow
import org.koin.dsl.module

class StoryRepositoryImpl(
    private val db: LocalClient,
    private val persistence: StoryPersistence
) : StoryRepository {

    override suspend fun addStory(body: StoryBody): ResultData<Boolean> {
        val (stringMap, photo) = body.asPartMap()
        val resultRemote = persistence.postStory(stringMap, photo)
        return if (resultRemote.succeeded) {
            resultRemote as ResultData.Success
        } else {
            resultRemote as ResultData.Failed
        }
    }

    @OptIn(ExperimentalPagingApi::class)
    override fun getStories(key: String): Flow<PagingData<StoryEntity>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 1
            ),
            remoteMediator = StoryRemoteMediator(db, persistence, key),
            pagingSourceFactory = {
                db.storyDao.getStoryWithKey()
            }
        ).flow
    }

    override suspend fun getStoriesLocation(): ResultData<List<StoryDomain>> {
        val resultRemote = persistence.storiesWithLocation()
        return if (resultRemote.succeeded) {
            resultRemote as ResultData.Success
            val data = resultRemote.data.asSequence().map { it.asDomain }.toList()
            ResultData.Success(data)
        } else {
            resultRemote as ResultData.Failed
        }
    }

    companion object {
        fun modelStory() = module {
            factory<StoryRepository> { StoryRepositoryImpl(get(), get()) }
        }
    }
}
