package com.f1k375.rycoapp.data.remote.story

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import com.f1k375.rycoapp.data.remote.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.dsl.module
import retrofit2.Response

class StoryPersistenceImpl(
    private val service: StoryService
) : StoryPersistence {

    override suspend fun postStory(
        stringMap: Map<String, RequestBody>,
        photo: MultipartBody.Part?
    ): ResultData<Boolean> =
        safeApiCall(
            call = {
                val response = service.postStory(stringMap, photo)
                if (response.isSuccessful) ResultData.Success(true)
                else errorResponseHandler(response)
            },
            errorMessage = "Failed to perform post story\nCrash on system"
        )

    override suspend fun stories(
        page: Int,
        limit: Int
    ): ResultData<BaseResponse<List<StoryResponse>>> =
        safeApiCall(
            call = {
                responsePagingHandler(
                    service.stories(
                        page,
                        limit
                    ) as Response<BaseResponse<List<StoryResponse>>>
                )
            },
            errorMessage = "Failed to perform get post story\nCrash on system"
        )

    override suspend fun storiesWithLocation(): ResultData<List<StoryResponse>> =
        safeApiCall(
            call = { responseHandler(service.storiesWithLocation() as Response<BaseResponse<List<StoryResponse>>>) },
            errorMessage = "Failde to perform get post story with location\nCrash on system"
        )


    companion object {
        fun moduleStory() = module {
            single { provideRetrofitService(get(), StoryService::class.java) }
            single<StoryPersistence> { StoryPersistenceImpl(get()) }
        }
    }
}
