package com.f1k375.rycoapp.data.models.request.story

import com.f1k375.rycoapp.helpers.extension.doThis
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

data class StoryBody(
    val photo: MultipartBody.Part? = null,
    val description: String,
    val lat: Double? = null,
    val lon: Double? = null
) {

    data class ParamRequestStory(
        val stringMap: Map<String, RequestBody>,
        val photo: MultipartBody.Part?
    )

    fun asPartMap(): ParamRequestStory {
        val stringMap = mutableMapOf<String, RequestBody>()
        description.isNotEmpty().doThis {
            stringMap["description"] = description.toRequestBody("text/plain".toMediaTypeOrNull())
        }
        lat?.let {
            stringMap["lat"] = lat.toString().toRequestBody("text/plain".toMediaTypeOrNull())
        }
        lon?.let {
            stringMap["lon"] = lon.toString().toRequestBody("text/plain".toMediaTypeOrNull())
        }
        return ParamRequestStory(stringMap, photo)

    }

}
