package com.f1k375.rycoapp.data.remote.story

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import com.f1k375.rycoapp.data.remote.ResultData
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface StoryPersistence {

    suspend fun postStory(
        stringMap: Map<String, RequestBody>,
        photo: MultipartBody.Part?
    ): ResultData<Boolean>

    suspend fun stories(page: Int, limit: Int): ResultData<BaseResponse<List<StoryResponse>>>
    suspend fun storiesWithLocation(): ResultData<List<StoryResponse>>

}
