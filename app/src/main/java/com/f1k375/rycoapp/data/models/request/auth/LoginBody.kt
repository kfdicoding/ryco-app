package com.f1k375.rycoapp.data.models.request.auth

data class LoginBody(
    val email: String,
    val password: String
)
