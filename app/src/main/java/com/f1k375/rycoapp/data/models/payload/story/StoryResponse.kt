package com.f1k375.rycoapp.data.models.payload.story

import com.google.gson.annotations.SerializedName

data class StoryResponse(

	@SerializedName("id")
	val id: String? = null,

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("photoUrl")
	val photoUrl: String? = null,

	@SerializedName("description")
	val description: String? = null,

	@SerializedName("createdAt")
	val createdAt: String? = null,


	@SerializedName("lon")
	val lon: Double? = null,

	@SerializedName("lat")
	val lat: Double? = null
)
