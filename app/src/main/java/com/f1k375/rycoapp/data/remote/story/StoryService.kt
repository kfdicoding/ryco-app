package com.f1k375.rycoapp.data.remote.story

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.story.PostStoryResponse
import com.f1k375.rycoapp.data.remote.Network.API_V1
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface StoryService {

    @Multipart
    @POST(API_V1 + "stories")
    suspend fun postStory(
        @PartMap stringMap: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part photo: MultipartBody.Part?
    ): Response<BaseResponse<JsonObject>>

    @GET(API_V1 + "stories")
    suspend fun stories(
        @Query("page") page: Int,
        @Query("size") limit: Int
    ): Response<PostStoryResponse>

    @GET(API_V1 + "stories?location=1")
    suspend fun storiesWithLocation(
    ): Response<PostStoryResponse>

}
