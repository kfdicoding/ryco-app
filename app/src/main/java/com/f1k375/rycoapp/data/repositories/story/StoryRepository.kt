package com.f1k375.rycoapp.data.repositories.story

import androidx.paging.PagingData
import com.f1k375.rycoapp.data.local.story.StoryEntity
import com.f1k375.rycoapp.data.models.request.story.StoryBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.domain.StoryDomain
import kotlinx.coroutines.flow.Flow

interface StoryRepository {
    suspend fun addStory(body: StoryBody): ResultData<Boolean>
    fun getStories(key: String): Flow<PagingData<StoryEntity>>
    suspend fun getStoriesLocation(): ResultData<List<StoryDomain>>
}
