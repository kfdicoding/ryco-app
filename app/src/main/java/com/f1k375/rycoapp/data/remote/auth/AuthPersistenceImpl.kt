package com.f1k375.rycoapp.data.remote.auth

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.auth.UserResponse
import com.f1k375.rycoapp.data.models.request.auth.LoginBody
import com.f1k375.rycoapp.data.models.request.auth.RegisterBody
import com.f1k375.rycoapp.data.remote.*
import org.koin.dsl.module
import retrofit2.Response

class AuthPersistenceImpl(
    private val service: AuthService
): AuthPersistence {

    override suspend fun login(body: LoginBody): ResultData<UserResponse> =
        safeApiCall(
            call = { responseHandler(service.login(body) as Response<BaseResponse<UserResponse>>) },
            errorMessage = "Failed to perform login\nCrash on System"
        )

    override suspend fun register(body: RegisterBody): ResultData<Boolean> =
        safeApiCall(
            call = {
                val response = service.register(body)
                if (response.isSuccessful) ResultData.Success(true)
                else errorResponseHandler(response)
            },
            errorMessage = "Failed to perform register\nCrash on system"
        )

    companion object{
        fun moduleAuth() = module {
            single { provideRetrofitService(get(), AuthService::class.java) }
            single<AuthPersistence> { AuthPersistenceImpl(get()) }
        }
    }
}
