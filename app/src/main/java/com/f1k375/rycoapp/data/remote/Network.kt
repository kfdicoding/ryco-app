package com.f1k375.rycoapp.data.remote

import com.f1k375.rycoapp.BuildConfig
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.java.KoinJavaComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Network {

    const val BASE_URL = "https://story-api.dicoding.dev/"
    const val API_V1 = "v1/"

    private val loginHelper by KoinJavaComponent.inject<AccountHelper>(AccountHelper::class.java)

    fun getRetrofit(okhttpClient: OkHttpClient, baseUrl: String): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okhttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun getOkhttp(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()

        if (!BuildConfig.IS_RELEASE) {
            val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
        }

        val connectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_3)
            .cipherSuites(
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
            ).build()

        okHttpClientBuilder.connectionSpecs(listOf(connectionSpec))

        okHttpClientBuilder.connectTimeout(20, TimeUnit.SECONDS)
        okHttpClientBuilder.readTimeout(20, TimeUnit.SECONDS)
        okHttpClientBuilder.writeTimeout(20, TimeUnit.SECONDS)

        okHttpClientBuilder.addInterceptor { chain ->
            val mOriRequest: Request = chain.request()
            val token = loginHelper.tokenAuth
            val request: Request =
                mOriRequest.newBuilder()
                    .addHeader("Authorization", "Bearer $token")
                    .addHeader("Accept", "application/json")
                    .method(mOriRequest.method, mOriRequest.body)
                    .build()
            chain.proceed(request)
        }

        return okHttpClientBuilder.build()
    }
}
