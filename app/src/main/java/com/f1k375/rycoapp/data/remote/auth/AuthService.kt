package com.f1k375.rycoapp.data.remote.auth

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.auth.LoginResponse
import com.f1k375.rycoapp.data.models.request.auth.LoginBody
import com.f1k375.rycoapp.data.models.request.auth.RegisterBody
import com.f1k375.rycoapp.data.remote.Network.API_V1
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST(API_V1+"login")
    suspend fun login(
        @Body body: LoginBody
    ): Response<LoginResponse>

    @POST(API_V1 + "register")
    suspend fun register(
        @Body body: RegisterBody
    ): Response<BaseResponse<JsonObject>>

}
