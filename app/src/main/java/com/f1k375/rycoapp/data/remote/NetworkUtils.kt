/*
 * Copyright 2018 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f1k375.rycoapp.data.remote

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.NothingResponse
import com.google.gson.Gson
import com.google.gson.JsonNull
import com.google.gson.JsonParser
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Wrap a suspending API [call] in try/catch. In case an exception is thrown, a [ResultData.Failed] is
 * created based on the [errorMessage].
 */
suspend fun <T : Any> safeApiCall(
    call: suspend () -> ResultData<T>,
    errorMessage: String
): ResultData<T> {
    return try {
        call()
    } catch (e: Exception) {
        // An exception was thrown when calling the API so we're converting this to an IOException
        e.printStackTrace()
        ResultData.Failed(exception = IOException(errorMessage + " : " + e.message, e))
    }
}

fun <T> provideRetrofitService(retrofit: Retrofit, service: Class<T>): T = retrofit.create(service)

fun <T : Any> responseHandler(
    response: Response<BaseResponse<T>>,
    alterResponse: T? = null
): ResultData<T> {
    return if (response.isSuccessful)
        successResponseHandler(alterResponse ?: response.body()?.data)
    else errorResponseHandler(response)
}

fun <T : Any> responsePagingHandler(response: Response<BaseResponse<T>>): ResultData<BaseResponse<T>> {
    return if (response.isSuccessful)
        successResponseHandler(response.body())
    else errorResponseHandler(response)
}

fun <T : Any> errorResponseHandler(response: Response<T>): ResultData.Failed {
    val errorResponse = response.errorBody()?.string()
    val data = errorMessage(errorResponse)
    return ResultData.Failed(data, HttpException(response))
}

private fun <T : Any> successResponseHandler(data: T?): ResultData<T> {
    return if (data != null) ResultData.Success(data)
    else ResultData.Failed(exception = IOException("Response Empty"))
}

fun createMultiPartBody(
    context: Context,
    fileSource: Uri?,
    formDataName: String
): MultipartBody.Part? {
    return if (fileSource != null) {
        val fileName = getFileName(fileSource, context)
        val contentType = context.contentResolver.getType(fileSource)
        val mediaType = contentType?.toMediaTypeOrNull()
        val inputStream = context.contentResolver.openInputStream(fileSource)
        val requestBody = inputStream?.readBytes()?.toRequestBody(mediaType)
        val multipartBody = requestBody?.let {
            MultipartBody.Part.createFormData(formDataName, fileName, it)
        }

        inputStream?.close()


        multipartBody
    } else null
}

private fun errorMessage(response: String?): BaseResponse<Nothing>? {
    val fileJsonResponse = try {
        JsonParser.parseString(response)
    } catch (e: Exception) {
        JsonNull.INSTANCE
    }
    return Gson().fromJson(fileJsonResponse, NothingResponse::class.java)
}

private fun getFileName(uri: Uri?, applicationContext: Context): String? {
    return try {
        if (uri != null) {
            applicationContext.contentResolver?.query(uri, null, null, null, null)
                ?.let { cursor ->
                    cursor.run {
                        if (moveToFirst()) getString(
                            getColumnIndex(OpenableColumns.DISPLAY_NAME).coerceAtLeast(
                                0
                            )
                        )
                        else null
                    }.also { cursor.close() }
                }
        } else
            null
    } catch (e: Exception) {
        null
    }
}
