package com.f1k375.rycoapp.data.repositories.auth

import com.f1k375.rycoapp.data.models.request.auth.LoginBody
import com.f1k375.rycoapp.data.models.request.auth.RegisterBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.auth.AuthPersistence
import com.f1k375.rycoapp.data.remote.succeeded
import com.f1k375.rycoapp.domain.UserDomain
import com.f1k375.rycoapp.domain.asDomain
import org.koin.dsl.module

class AuthRepositoryImpl(
    private val persistence: AuthPersistence
): AuthRepository {

    override suspend fun login(email: String, password: String): ResultData<UserDomain> {
        val remoteResult = persistence.login(
            LoginBody(email, password)
        )
        return if (remoteResult.succeeded){
            remoteResult as ResultData.Success
            val data = remoteResult.data.asDomain
            ResultData.Success(data)
        }else{
            remoteResult as ResultData.Failed
        }
    }

    override suspend fun register(
        name: String,
        email: String,
        password: String
    ): ResultData<Boolean> {
        val remoteResult = persistence.register(
            RegisterBody(
                name, email, password
            )
        )
        return if (remoteResult.succeeded){
            remoteResult as ResultData.Success
        }else{
            remoteResult as ResultData.Failed
        }
    }

    companion object{
        fun moduleAuth() = module {
            factory<AuthRepository> { AuthRepositoryImpl(get()) }
        }
    }
}
