package com.f1k375.rycoapp.data.models.request.auth

data class RegisterBody(
    val name: String,
    val email: String,
    val password: String
)
