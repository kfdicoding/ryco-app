package com.f1k375.rycoapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.f1k375.rycoapp.data.local.remotekey.RemoteKey
import com.f1k375.rycoapp.data.local.remotekey.RemoteKeyDao
import com.f1k375.rycoapp.data.local.story.StoryDao
import com.f1k375.rycoapp.data.local.story.StoryEntity

@Database(
    entities = [
        StoryEntity::class,
        RemoteKey::class,
    ], version = 1, exportSchema = false
)
abstract class LocalClient : RoomDatabase() {
    abstract val storyDao: StoryDao
    abstract val remoteKeyDao: RemoteKeyDao
}