package com.f1k375.rycoapp.data.repositories.auth

import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.domain.UserDomain

interface AuthRepository {

    suspend fun login(email: String, password: String): ResultData<UserDomain>
    suspend fun register(name: String, email: String, password: String): ResultData<Boolean>

}
