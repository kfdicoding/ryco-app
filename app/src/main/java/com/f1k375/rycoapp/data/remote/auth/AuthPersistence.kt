package com.f1k375.rycoapp.data.remote.auth

import com.f1k375.rycoapp.data.models.payload.auth.UserResponse
import com.f1k375.rycoapp.data.models.request.auth.LoginBody
import com.f1k375.rycoapp.data.models.request.auth.RegisterBody
import com.f1k375.rycoapp.data.remote.ResultData

interface AuthPersistence {

    suspend fun login(body: LoginBody): ResultData<UserResponse>
    suspend fun register(body: RegisterBody): ResultData<Boolean>

}
