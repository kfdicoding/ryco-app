package com.f1k375.rycoapp.data.models.payload.auth

import com.f1k375.rycoapp.data.models.BaseResponse
import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("loginResult")
    override val data: UserResponse?
): BaseResponse<UserResponse>()
