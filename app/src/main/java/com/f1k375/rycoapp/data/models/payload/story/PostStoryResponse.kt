package com.f1k375.rycoapp.data.models.payload.story

import com.f1k375.rycoapp.data.models.BaseResponse
import com.google.gson.annotations.SerializedName

data class PostStoryResponse(
    @SerializedName("listStory")
    override val data: List<StoryResponse>?
): BaseResponse<List<StoryResponse>>()
