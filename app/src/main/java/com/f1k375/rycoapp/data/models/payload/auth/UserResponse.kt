package com.f1k375.rycoapp.data.models.payload.auth

data class UserResponse(
    val userId: String?,
    val name: String?,
    val token: String?
)
