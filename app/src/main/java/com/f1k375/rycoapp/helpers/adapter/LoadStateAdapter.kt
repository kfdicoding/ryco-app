package com.f1k375.rycoapp.helpers.adapter

import android.view.ViewGroup
import androidx.paging.LoadState

class LoadStateAdapter(
    private val retry: () -> Unit
) : androidx.paging.LoadStateAdapter<ViewHolderLoadingAdapter>() {

    override fun onBindViewHolder(holder: ViewHolderLoadingAdapter, loadState: LoadState) {
        holder.setState(loadState, retry)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): ViewHolderLoadingAdapter {
        return ViewHolderLoadingAdapter.initiation(parent)
    }

}