package com.f1k375.rycoapp.helpers.sharedpreference

import android.content.Context
import androidx.core.content.edit
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.domain.UserDomain

class AccountHelper(
    mContext: Context
) : BaseSharedPreference(mContext, mContext.getString(R.string.preference_login_app)) {

    fun setLocationActiveOrNote(status: Boolean) {
        mSharedPreference.edit {
            putBoolean(getResourceString(R.string.preference_login_app_location_feature), status)
        }
    }

    val isLocationOn: Boolean
        get() = mSharedPreference.getBoolean(
            getResourceString(R.string.preference_login_app_location_feature),
            false
        )

    fun setLoginData(data: UserDomain) {
        setAuthToken(data.token)
        setUserData(data)
    }

    private fun setAuthToken(token: String) =
        saveContentString(R.string.preference_login_app_token, token)

    private fun setUserData(data: UserDomain) {
        saveContentString(R.string.preference_login_app_user, data.toString())
    }

    val userData: UserDomain?
        get() {
            val stringData = getContentString(R.string.preference_login_app_user)
            return if (stringData.isEmpty()) null
            else UserDomain.fromString(stringData)
        }

    val tokenAuth
        get() = getContentString(R.string.preference_login_app_token)

    val isLogin
        get() = tokenAuth.isNotEmpty()

    fun logout() {
        clearData()
    }

}
