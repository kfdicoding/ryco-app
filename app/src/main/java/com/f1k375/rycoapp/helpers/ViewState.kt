package com.f1k375.rycoapp.helpers

sealed class ViewState<out T: Any> {
    data class DataLoaded<out T: Any>(val data: T) : ViewState<T>()
    object Loading : ViewState<Nothing>()
    data class Error(val message: String, val errorCode: Int? = null) : ViewState<Nothing>()
}
