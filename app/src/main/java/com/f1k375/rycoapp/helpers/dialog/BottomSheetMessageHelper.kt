package com.f1k375.rycoapp.helpers.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.LayoutBottomSheetMessageBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Bottom sheet message
 *
 * default bottom sheet for showing info
 *
 * remember to always use this class, or modify as desire but always use this
 * for more easy to maintain
 */
class BottomSheetMessageHelper : BottomSheetDialogFragment() {

    private val binding: LayoutBottomSheetMessageBinding by lazy {
        LayoutBottomSheetMessageBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.MyBottomSheetDialogTheme)

        if (savedInstanceState != null) {
            title = savedInstanceState.getString(EXTRA_MESSAGE_TITLE) ?: ""
            desc = savedInstanceState.getString(EXTRA_MESSAGE_DESC) ?: ""
            typeMessage = savedInstanceState.getString(EXTRA_MESSAGE_TYPE) ?: ""
        } else {
            title = arguments?.getString(EXTRA_MESSAGE_TITLE) ?: ""
            desc = arguments?.getString(EXTRA_MESSAGE_DESC) ?: ""
            typeMessage = arguments?.getString(EXTRA_MESSAGE_TYPE) ?: ""
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(EXTRA_MESSAGE_TITLE, title)
            putString(EXTRA_MESSAGE_DESC, desc)
            putString(EXTRA_MESSAGE_TYPE, typeMessage)
        }
        super.onSaveInstanceState(outState)
    }

    private var title = ""
    private var desc = ""
    private var typeMessage = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)
        try {
            dialog.setOnShowListener {
                val bottomSheetDialog = it as BottomSheetDialog
                val bottomSheet =
                    bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                val behavior = BottomSheetBehavior.from(bottomSheet!!)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        } catch (_: Exception) {
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imageResource = when (typeMessage) {
            SUCCESS_TASK_MESSAGE -> R.raw.lottie_success
            FAIL_TASK_MESSAGE -> R.raw.lottie_failed
            else -> R.raw.lottie_success
        }
        binding.run {
            mImageStatus.setAnimation(imageResource)
            mTextMessageTitle.text = title
            mTextMessageDesc.text = desc
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (activity != null && activity is DialogInterface.OnDismissListener) {
            (activity as DialogInterface.OnDismissListener).onDismiss(dialog)
        } else if (parentFragment != null && parentFragment is DialogInterface.OnDismissListener) {
            (parentFragment as DialogInterface.OnDismissListener).onDismiss(dialog)
        }
    }

    companion object {
        const val SUCCESS_TASK_MESSAGE = "typePopUpSuccessTaskMessage"
        const val FAIL_TASK_MESSAGE = "typePopUpFailTask"
        private const val EXTRA_MESSAGE_TITLE = "extraMessageTitle"
        private const val EXTRA_MESSAGE_DESC = "extraMessageDesc"
        private const val EXTRA_MESSAGE_TYPE = "extraMessageType"

        private fun mOnSavedInstanceFragment(
            type: String,
            title: String,
            desc: String,
        ): BottomSheetMessageHelper {
            val fragment = BottomSheetMessageHelper()
            val mArgs = Bundle()

            mArgs.run {
                putString(EXTRA_MESSAGE_TITLE, title)
                putString(EXTRA_MESSAGE_DESC, desc)
                putString(EXTRA_MESSAGE_TYPE, type)
            }
            fragment.arguments = mArgs

            return fragment
        }

        fun showBottomSheetDialogMessage(
            type: String,
            title: String,
            desc: String,
            fm: FragmentManager
        ) {
            val bottomSheet = mOnSavedInstanceFragment(
                type,
                title,
                desc,
            )
            bottomSheet.show(fm, BottomSheetMessageHelper::class.qualifiedName)
        }
    }
}
