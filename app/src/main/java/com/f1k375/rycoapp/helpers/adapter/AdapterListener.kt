package com.f1k375.rycoapp.helpers.adapter

import android.view.View

class AdapterListener {
    interface OnClickItem {
        fun onClickItem(data: Any?, position: Int, view: View? = null)
    }
}
