package com.f1k375.rycoapp.helpers.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

open class BaseSharedPreference(
    private val mContext: Context,
    spName: String
) {

    protected val mSharedPreference: SharedPreferences =
        mContext.getSharedPreferences(spName, Context.MODE_PRIVATE)

    protected fun getResourceString(resource: Int): String {
        return mContext.getString(resource)
    }

    protected fun saveContentString(mKeyName: Int, mData: String) {
        val stringResource = getResourceString(mKeyName)
        mSharedPreference.edit {
            putString(stringResource, mData)
        }
    }

    protected fun getContentString(mKeyName: Int): String {
        return mSharedPreference.getString(getResourceString(mKeyName), "") ?: ""
    }

    protected fun clearData() {
        mSharedPreference.edit {
            clear()
        }
    }

}
