package com.f1k375.rycoapp.helpers.dialog

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.databinding.LayoutAlertMessageBinding
import com.f1k375.rycoapp.utils.DialogUtil


/**
 * Messages Dialog Helper
 *
 * this class used for showing pop up Alert dialog
 *
 * always use this rather then creating new one in each activity or fragment,
 * modify it as needed
 *
 */
class MessagesDialogHelper constructor(
    private val mContext: Context
) {

    private val layoutAlertMessageBinding: LayoutAlertMessageBinding by lazy {
        LayoutAlertMessageBinding.inflate(LayoutInflater.from(mContext))
    }

    private var messageAlert:AlertDialog? = null

    init {
        messageAlert = AlertDialog
            .Builder(mContext)
            .setCancelable(false)
            .create()
        messageAlert?.setView(layoutAlertMessageBinding.root)
        messageAlert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        messageAlert?.setOnDismissListener {
            it.cancel()
        }
        messageAlert?.setOnShowListener {
            DialogUtil.alterDialogWidth(messageAlert!!, mContext as Activity, 86)
        }
    }

    private fun setMessage(title: String, message: String) {
        layoutAlertMessageBinding.run {
            mMessageTitle.text = title
            mMessageBody.text = message
        }
    }

    private fun getStringResource(resourceId: Int): String {
        return mContext.getString(resourceId)
    }

    /**
     * Show message alert
     *
     * showing alert with single button on it
     *
     * @param action function that will be performed when action button clicked
     * @param alterOnExit function that need to be performed when exit button clicked
     */
    fun showMessageAlert(
        mTitle: String,
        mMessage: String,
        action: ()->Unit = {},
        alterOnExit: ()-> Unit = {}
    ) {
        if (!(mContext as Activity).isDestroyed) {

            layoutAlertMessageBinding.run {
                setMessage(mTitle, mMessage)

                layoutButtonPositiveAlert.isVisible = true
                layoutButtonNegativeAlert.isVisible = false

                buttonNoPositive.isVisible = false

                buttonYesPositive.text = "OK"
                buttonYesPositive.setOnClickListener {
                    action()
                    messageAlert?.dismiss()
                }

                mImageExit.setOnClickListener {
                    alterOnExit()
                    messageAlert?.dismiss()
                }
            }

            messageAlert?.show()
        }
    }

    /**
     * Show message action to choose alert
     *
     * showing alert with two button on in, the views of buttons can be change depends on [isDangerAction] value
     *
     * @param isDangerAction if true then negative button will be highlight, if false then the positive button which will be highlighted
     * @param mYesFunction function that will be performed when positive button clicked
     * @param mNoFunction function that will be performed when negative button clicked
     * @param alterOnExit function that need to be performed when exit button clicked
     */
    fun showMessageActionToChooseAlert(
        mTitle: String,
        mMessage: String,
        isDangerAction: Boolean = false,
        mYesFunction: () -> Unit,
        mNoFunction: () -> Unit = {},
        alterOnExit: ()-> Unit = {}
    ) {

        if (!(mContext as Activity).isDestroyed) {

            layoutAlertMessageBinding.run {
                setMessage(mTitle, mMessage)

                if (isDangerAction) {
                    layoutButtonNegativeAlert.isVisible = true
                    layoutButtonPositiveAlert.isVisible = false

                    buttonNoNegative.visibility = View.VISIBLE

                    buttonYesNegative.text = getStringResource(R.string.text_yes_action)
                    buttonYesNegative.setOnClickListener {
                        messageAlert?.dismiss()
                        mYesFunction()
                    }

                    buttonNoNegative.text = getStringResource(R.string.text_no_action)
                    buttonNoNegative.setOnClickListener {
                        messageAlert?.dismiss()
                        mNoFunction()
                    }
                }else{
                    layoutButtonPositiveAlert.isVisible = true
                    layoutButtonNegativeAlert.isVisible = false

                    buttonNoPositive.visibility = View.VISIBLE

                    buttonYesPositive.text = getStringResource(R.string.text_yes_action)
                    buttonYesPositive.setOnClickListener {
                        messageAlert?.dismiss()
                        mYesFunction()
                    }

                    buttonNoPositive.text = getStringResource(R.string.text_no_action)
                    buttonNoPositive.setOnClickListener {
                        messageAlert?.dismiss()
                        mNoFunction()
                    }
                }

                mImageExit.setOnClickListener {
                    alterOnExit()
                    messageAlert?.dismiss()
                }
            }

            messageAlert?.show()
        }
    }
}
