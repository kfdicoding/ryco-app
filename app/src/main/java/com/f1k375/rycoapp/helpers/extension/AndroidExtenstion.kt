package com.f1k375.rycoapp.helpers.extension

import android.app.Activity
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Activity.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        applicationContext, message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        requireContext().applicationContext, message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}
