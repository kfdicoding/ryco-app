package com.f1k375.rycoapp.helpers.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.f1k375.rycoapp.databinding.LayoutLoadStateAdapterBinding
import com.f1k375.rycoapp.helpers.extension.alsoNotDoThis

class ViewHolderLoadingAdapter(private val mBinding: LayoutLoadStateAdapterBinding) :
    RecyclerView.ViewHolder(mBinding.root) {

    fun setState(loadState: LoadState, retry: () -> Unit) {
        mBinding.run {
            loadingLayout.root.isVisible = loadState is LoadState.Loading
            errorLayout.root.isVisible = (loadState is LoadState.Error).alsoNotDoThis {
                errorLayout.buttonRetry.setOnClickListener {
                    retry.invoke()
                }
            }
        }
    }

    companion object {
        fun initiation(parent: ViewGroup) = ViewHolderLoadingAdapter(
            LayoutLoadStateAdapterBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

}
