package com.f1k375.rycoapp.helpers.location

import android.annotation.SuppressLint
import android.app.Application
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import com.google.android.gms.location.*
import kotlinx.coroutines.flow.*

class GeoLocationDataSource(
    private val context: Application
) {

    companion object {
        private const val REQUEST_INTERVAL = 15000L
        private const val REQUEST_FASTEST_INTERVAL = 5000L
        private const val EXPIRATION_TIME = 30000L
    }

    private val fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)
    private val locationRequest: LocationRequest = LocationRequest.create().apply {
        interval = REQUEST_INTERVAL
        fastestInterval = REQUEST_FASTEST_INTERVAL
        maxWaitTime = REQUEST_INTERVAL + REQUEST_FASTEST_INTERVAL
        priority = Priority.PRIORITY_BALANCED_POWER_ACCURACY
        setExpirationDuration(EXPIRATION_TIME)
    }

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            Log.d("location-get", "location data : $result")
            result.locations.forEach(::sendLocation)
        }
    }

    private val locationData = MutableLiveData<Location>()
    val locationObservable: Flow<Location>
        get() = locationData.asFlow()
            .onStart { startLocationUpdates() }
            .cancellable()
            .onCompletion {
                stopLocationUpdates()
            }
            .filterNot { it.latitude == 0.0 || it.longitude == 0.0 }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.lastLocation.addOnSuccessListener(::sendLocation).addOnFailureListener {
            requireUpdate()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requireUpdate() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        ).addOnFailureListener {
            val locationManager = getSystemService(context, LocationManager::class.java)
            val lastLocation = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            sendLocation(lastLocation)
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun sendLocation(location: Location?) {
        if (location != null) location.let { locationData.postValue(it) }
        else {
            updateLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun updateLocation() {
        stopLocationUpdates()
        requireUpdate()
    }


}