package com.f1k375.rycoapp.helpers

import androidx.room.Room
import com.f1k375.rycoapp.data.local.LocalClient
import com.f1k375.rycoapp.data.remote.Network.BASE_URL
import com.f1k375.rycoapp.data.remote.Network.getOkhttp
import com.f1k375.rycoapp.data.remote.Network.getRetrofit
import com.f1k375.rycoapp.data.remote.auth.AuthPersistenceImpl
import com.f1k375.rycoapp.data.remote.story.StoryPersistenceImpl
import com.f1k375.rycoapp.data.repositories.auth.AuthRepositoryImpl
import com.f1k375.rycoapp.data.repositories.story.StoryRepositoryImpl
import com.f1k375.rycoapp.helpers.location.GeoLocationDataSource
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.ui.auth.AuthViewModel
import com.f1k375.rycoapp.ui.story.StoryViewModel
import org.koin.dsl.module

object ModuleApp {

    private fun mainModule() = module {
        single { AccountHelper(get()) }
        single { GeoLocationDataSource(get()) }
    }

    private fun networkModules() = module {
        single { getOkhttp() }
        single { getRetrofit(get(), BASE_URL) }
    }

    private fun localClientModule() = module {
        single {
            Room.databaseBuilder(
                get(),
                LocalClient::class.java,
                "ryco_app.db"
            ).fallbackToDestructiveMigration().build()
        }
    }

    private fun persistenceModule() = listOf(
        AuthPersistenceImpl.moduleAuth(),
        StoryPersistenceImpl.moduleStory()
    )

    private fun repositoryModule() = listOf(
        AuthRepositoryImpl.moduleAuth(),
        StoryRepositoryImpl.modelStory()
    )

    private fun viewModelModule() = listOf(
        AuthViewModel.moduleInstance(),
        StoryViewModel.moduleInstance()
    )

    val listModules
        get() = listOf(
            mainModule(),
            networkModules(),
            localClientModule(),
            *persistenceModule().toTypedArray(),
            *repositoryModule().toTypedArray(),
            *viewModelModule().toTypedArray()
        )

}
