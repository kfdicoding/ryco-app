package com.f1k375.rycoapp.helpers.extension

import android.os.Looper
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*


fun Boolean.doThis(job: () -> Unit) {
    if (this)
        job()
}

fun Boolean.alsoDoThis(job: () -> Unit): Boolean {
    if (this)
        job()
    return this
}

fun Boolean.notDoThis(job: () -> Unit) {
    if (!this)
        job()
}

fun Boolean.alsoNotDoThis(job: () -> Unit): Boolean {
    if (!this)
        job()
    return this
}

fun String.asNameInitial(): String {
    val nameSplit = this.split(" ")
    return if (nameSplit.size > 1)
        nameSplit.firstOrNull()?.firstOrNull()?.uppercase() +
                nameSplit.lastOrNull()?.firstOrNull()?.uppercase()
    else
        nameSplit.firstOrNull()?.firstOrNull()?.uppercase() +
                nameSplit.firstOrNull()?.firstOrNull()?.uppercase()
}

fun String.toColorHex(): String {
    var hash = 0
    this.forEach {
        hash = it.hashCode() + ((hash.shl(5)) - hash)
        hash = hash.and(hash)
    }

    var color = "#"
    for (i in 0..2) {
        val value = hash shr i * 8 and 255
        color += ("00" + value.toString(16)).takeLast(2)
    }
    return color
}

internal fun checkMainThread() {
    check(Looper.myLooper() == Looper.getMainLooper()) {
        "Expected to be called on the main thread but was " + Thread.currentThread().name
    }
}

@FlowPreview
@ExperimentalCoroutinesApi
fun EditText.onTextChangesSearchTypePurpose(
    scope: CoroutineScope,
    delay: Long = 300,
    action: (key: CharSequence) -> Unit
) {
    callbackFlow {
        checkMainThread()
        val listener = doAfterTextChanged {
            trySend(it).isSuccess
        }
        addTextChangedListener(listener)
        awaitClose { removeTextChangedListener(listener) }
    }.filterNotNull()
        .debounce(delay)
        .onEach { action(it) }
        .launchIn(scope)
}
