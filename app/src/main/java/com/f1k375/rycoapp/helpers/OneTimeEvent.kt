package com.f1k375.rycoapp.helpers

import java.util.concurrent.atomic.AtomicBoolean

/**
 * @author aminography
 *
 * for setting a object to be performed only one each event that observe that object
 */
class OneTimeEvent<T>(
    private val value: T
) {

    private val isConsumed = AtomicBoolean(false)

    internal fun getValue(): T? =
        if (isConsumed.compareAndSet(false, true)) value
        else null
}

fun <T> T.toOneTimeEvent() =
    OneTimeEvent(this)

fun <T> OneTimeEvent<T>.consume(block: (T) -> Unit): T? =
    getValue()?.also(block)
