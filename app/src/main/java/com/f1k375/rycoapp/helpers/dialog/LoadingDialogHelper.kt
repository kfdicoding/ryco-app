package com.f1k375.rycoapp.helpers.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.WindowInsets
import android.view.WindowManager
import androidx.core.content.res.ResourcesCompat
import com.f1k375.rycoapp.databinding.LayoutLoadingDialogBinding

/**
 * Loading dialog
 *
 * this class is use for showing a loading on screen using Dialog view that cannot be canceled
 */
class LoadingDialogHelper(
    private val mContext: Context
) {

    private val layoutLoadingDialogBinding by lazy {
        LayoutLoadingDialogBinding.inflate(LayoutInflater.from(mContext))
    }
    private var loadingDialog: Dialog? = null

    init {
        loadingDialog = Dialog(mContext)
        loadingDialog?.setContentView(layoutLoadingDialogBinding.root)
        loadingDialog?.setCancelable(false)

        loadingDialog?.setOnShowListener {
            fun widthScreen(): Int{
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    val windowMatrix = (mContext as Activity).windowManager.currentWindowMetrics
                    val inset = windowMatrix.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
                    windowMatrix.bounds.width() - inset.left - inset.right
                } else{
                    val displayMetrics = DisplayMetrics()
                    (mContext as Activity).windowManager.defaultDisplay.getRealMetrics(displayMetrics)
                    displayMetrics.widthPixels
                }
            }

            val displayWidth = widthScreen()
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(loadingDialog?.window?.attributes)

            val dialogWindowWidth = (displayWidth * 0.75f).toInt()
            layoutParams.width = dialogWindowWidth

            loadingDialog?.window?.attributes = layoutParams
            loadingDialog?.window?.setBackgroundDrawable(ResourcesCompat.getDrawable(mContext.resources, android.R.color.transparent, null))
        }

    }

    /**
     * Show loading dialog
     *
     * show loading that cannot be canceled
     */
    fun showDialog(){
        loadingDialog?.show()
    }

    fun hideDialog(){
        loadingDialog?.dismiss()
    }

}
