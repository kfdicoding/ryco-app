package com.f1k375.rycoapp.domain

import android.content.Context
import android.os.Parcelable
import com.f1k375.rycoapp.R
import com.f1k375.rycoapp.data.local.story.StoryEntity
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import kotlinx.parcelize.Parcelize
import java.lang.ref.WeakReference
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

@Parcelize
data class StoryDomain(
    val id: String,
    val name: String,
    val description: String,
    val photo: String,
    val createdAt: String,
    val latitude: Double,
    val longitude: Double,
) : Parcelable {
    fun postTime(context: WeakReference<Context>): String {
        return try {
            var datePost = LocalDateTime.parse(
                createdAt,
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
            )
            datePost = datePost.plusHours(7)
            val dateNow = LocalDateTime.now()
            val diffDate = Period.between(datePost.toLocalDate(), dateNow.toLocalDate())
            when {
                diffDate.years > 0 -> datePost.format(
                    DateTimeFormatter.ofPattern("dd MMM yyyy").withLocale(
                        Locale.getDefault()
                    )
                )
                (diffDate.months > 0) or (diffDate.days > 7) -> datePost.format(
                    DateTimeFormatter.ofPattern(
                        "dd MMM"
                    ).withLocale(Locale.getDefault())
                )
                diffDate.days in 1..7 -> context.get()
                    ?.getString(R.string.text_days_post, diffDate.days) ?: ""
                datePost.until(dateNow, ChronoUnit.HOURS) > 0 -> context.get()
                    ?.getString(R.string.text_hours_post, datePost.until(dateNow, ChronoUnit.HOURS))
                    ?: ""
                datePost.until(dateNow, ChronoUnit.MINUTES) > 0 -> context.get()?.getString(
                    R.string.text_minutes_post,
                    datePost.until(dateNow, ChronoUnit.MINUTES)
                ) ?: ""
                datePost.until(dateNow, ChronoUnit.SECONDS) > 0 -> context.get()?.getString(
                    R.string.text_seconds_post,
                    datePost.until(dateNow, ChronoUnit.SECONDS)
                ) ?: ""
                else -> "a few second ago"
            }
        } catch (e: Exception) {
            "Format error"
        }
    }

    fun postTimeFull(): String {
        val datePost = LocalDateTime.parse(
            createdAt,
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        )
        return datePost.format(
            DateTimeFormatter.ofPattern("EEEE, dd MMM yyyy kk:mm").withLocale(
                Locale.getDefault()
            )
        )
    }
}

val StoryResponse.asDomain
    get() = StoryDomain(
        id = id ?: "",
        name = name ?: "",
        description = description ?: "",
        photo = photoUrl ?: "",
        createdAt = createdAt ?: "",
        latitude = lat ?: 0.0,
        longitude = lon ?: 0.0
    )

val StoryEntity.asDomain
    get() = StoryDomain(
        id = id,
        name = name,
        description = description,
        photo = photo,
        createdAt = createdAt,
        latitude = latitude,
        longitude = longitude
    )
