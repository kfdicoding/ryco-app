package com.f1k375.rycoapp.domain

import com.f1k375.rycoapp.data.models.BaseResponse

data class ErrorDomain(
    val isError: Boolean,
    val message: String,
    var errorCode: Int = 0,
)

fun <T> BaseResponse<T>.asErrorDomain() =
    ErrorDomain(
        isError,
        message?:"",
        errorCode
    )
