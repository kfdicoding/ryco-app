package com.f1k375.rycoapp.domain

import com.f1k375.rycoapp.data.models.BaseResponse

data class PaginationDomain(
    val isError: Boolean,
    val message: String,
    var data: Any?,
    val isLastPage: Boolean = (data as List<Any>).isEmpty()
)

fun <T> BaseResponse<T>.asPaginationDomain() =
    PaginationDomain(
        isError,
        message?:"",
        data = data,
    )
