package com.f1k375.rycoapp.domain

import com.f1k375.rycoapp.data.models.payload.auth.UserResponse
import com.google.gson.Gson

data class UserDomain(
    val id: String,
    val name: String,
    val token: String
){
    override
    fun toString(): String {
        return Gson().toJson(this.copy())
    }

    companion object {
        fun fromString(data: String): UserDomain? = try {
            Gson().fromJson(data, UserDomain::class.java)
        } catch (e: Exception) {
            null
        }
    }
}

val UserResponse.asDomain: UserDomain
get() = UserDomain(
    id = userId ?: "",
    name = name ?: "",
    token = token ?: ""
)
