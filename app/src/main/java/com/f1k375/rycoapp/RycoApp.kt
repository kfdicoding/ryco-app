package com.f1k375.rycoapp

import android.app.Application
import com.f1k375.rycoapp.helpers.ModuleApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class RycoApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            modules(module { single<Application>{this@RycoApp}})
            modules(ModuleApp.listModules)
        }
    }

}
