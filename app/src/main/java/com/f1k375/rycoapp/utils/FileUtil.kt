package com.f1k375.rycoapp.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

object FileUtil {

    private val timestamp: String
    get() = SimpleDateFormat(
        "dd-MMM-yyyy-kk-mm-ss",
        Locale.getDefault(),
    ).format(System.currentTimeMillis())

    fun createTempFile(context: Context): File {
        val directory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(timestamp, ".jpg", directory)
    }

    fun uriToFile(context: Context, uri: Uri): File{
        val contentResolver = context.contentResolver
        val file = createTempFile(context)
        val inputStream = contentResolver.openInputStream(uri)
        val outputStream = FileOutputStream(file)

        inputStream.use {input->
            outputStream.use {output->
                input?.copyTo(output)
            }
        }

        outputStream.flush()
        outputStream.close()

        inputStream?.close()

        return file
    }

    fun fileToUri(context: Context,file: File ): Uri{
        return  FileProvider.getUriForFile(
            context,
            context.applicationContext.packageName + ".provider",
            file
,        )
    }

    fun reduceFileImage(file: File, limitSize: Int = 1000000): File{
        val bitmap = BitmapFactory.decodeFile(file.path)

        var compressQuality = 100
        var fileSize: Int

        do {
            val outputStream = ByteArrayOutputStream()

            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, outputStream)
            val byteArray = outputStream.toByteArray()
            fileSize = byteArray.size
            compressQuality -=10

            outputStream.close()
        }while(fileSize > limitSize)

        bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, FileOutputStream(file))

        return file
    }

}
