package com.f1k375.rycoapp.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

object RegexUtils {

    fun isPasswordValid(password: String): Boolean{
        // minimum 6 character with at least contains 1 lowercase latter, 1 uppercase latter, and 1 number
        val regPass = "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{5,})\\S\$"
        val inputPass: CharSequence = password
        val pattern = Pattern.compile(regPass)
        val matcher = pattern.matcher(inputPass)

        return matcher.matches()
    }

    fun isEmailValid(email: String): Boolean{
        val regEmail = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
        val inputStr: CharSequence = email
        val pattern: Pattern = Pattern.compile(regEmail, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(inputStr)
        return matcher.matches()
    }

}
