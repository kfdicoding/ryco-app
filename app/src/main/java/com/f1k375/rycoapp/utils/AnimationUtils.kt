package com.f1k375.rycoapp.utils

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.view.View

/**
 * Animation utils
 *
 * some animation need to be called when view is already shown
 * (animation that get value from its view)
 * considering running animation inside view.post to run properly
 *
 */
object AnimationUtils {

    fun slideInLeftToRight(view: View, duration: Long = 1500): ValueAnimator {
        return ObjectAnimator.ofPropertyValuesHolder(view,
            PropertyValuesHolder.ofFloat(View.ALPHA, 0f,1f),
            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, (-view.width.toFloat()/2),0f)
        ).setDuration(duration)
    }

    fun slideInRightToLeft(view: View, duration: Long = 1500): ValueAnimator {
        return ObjectAnimator.ofPropertyValuesHolder(view,
            PropertyValuesHolder.ofFloat(View.ALPHA, 0f,1f),
            PropertyValuesHolder.ofFloat(View.TRANSLATION_X, (view.width.toFloat()/2),0f)
        ).setDuration(duration)
    }

    fun slideInBottomToTop(view: View, duration: Long = 1500): ValueAnimator {
        return ObjectAnimator.ofPropertyValuesHolder(view,
            PropertyValuesHolder.ofFloat(View.ALPHA, 0f,1f),
            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, (view.height.toFloat()/2),0f)
        ).setDuration(duration)
    }

    fun slideInTopToBottom(view: View, duration: Long = 1500): ValueAnimator {
        return ObjectAnimator.ofPropertyValuesHolder(view,
            PropertyValuesHolder.ofFloat(View.ALPHA, 0f,1f),
            PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, (-view.height.toFloat()/2),0f)
        ).setDuration(duration)
    }

    fun fadeIn(view: View, duration: Long = 2000): ObjectAnimator {
        return ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f).setDuration(duration)
    }
}
