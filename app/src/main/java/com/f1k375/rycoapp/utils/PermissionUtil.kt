package com.f1k375.rycoapp.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

object PermissionUtil {

    fun isPermissionOpenStorage(mContext: Context): Boolean {
        return (ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    fun isPermissionOpenCamera(mContext: Context): Boolean {
        return (ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED)
    }

    fun permissionOpenStorage() = Manifest.permission.READ_EXTERNAL_STORAGE
    fun permissionCamera() = Manifest.permission.CAMERA

    fun isPermissionLocationGranted(context: Context) = ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED

    fun permissionLocation() = Manifest.permission.ACCESS_FINE_LOCATION

}
