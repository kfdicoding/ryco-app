package com.f1k375.rycoapp.utils

import android.app.Activity
import android.app.Dialog
import android.os.Build
import android.util.DisplayMetrics
import android.view.WindowInsets
import android.view.WindowManager

object DialogUtil {

    fun alterDialogWidth(dialog: Dialog, activity: Activity,  widthPercentage: Int){
        fun widthScreen(): Int{
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val windowMatrix = activity.windowManager.currentWindowMetrics
                val inset = windowMatrix.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
                windowMatrix.bounds.width() - inset.left - inset.right
            } else{
                val displayMetrics = DisplayMetrics()
                activity.windowManager.defaultDisplay.getRealMetrics(displayMetrics)
                displayMetrics.widthPixels
            }
        }

        val displayWidth = widthScreen()
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.window?.attributes)

        val dialogWindowWidth = (displayWidth * widthPercentage.toFloat().div(100)).toInt()
        layoutParams.width = dialogWindowWidth

        dialog.window?.attributes = layoutParams
    }

}
