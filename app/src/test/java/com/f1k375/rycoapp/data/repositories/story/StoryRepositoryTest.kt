package com.f1k375.rycoapp.data.repositories.story

import com.f1k375.rycoapp.data.local.LocalClient
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import com.f1k375.rycoapp.data.models.request.story.StoryBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.story.StoryPersistence
import com.f1k375.rycoapp.domain.asDomain
import com.f1k375.rycoapp.utils.DummyData
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class StoryRepositoryTest {

    @Mock
    private lateinit var mockStoryPersistence: StoryPersistence

    @Mock
    private lateinit var dbLocal: LocalClient
    private lateinit var storyRepoTest: StoryRepository

    @Before
    fun setInitSetUp() {
        storyRepoTest = StoryRepositoryImpl(dbLocal, mockStoryPersistence)
    }

    @Test
    fun `success add story`() = runBlocking {
        val expectedRepo = ResultData.Success(true)

        `when`(
            mockStoryPersistence.postStory(
                emptyMap(), null
            )
        ).thenReturn(expectedRepo)
        val actualRepo = storyRepoTest.addStory(StoryBody(description = ""))

        Assert.assertTrue(actualRepo is ResultData.Success)
        Assert.assertEquals(expectedRepo, actualRepo)
    }

    @Test
    fun `failed add story unsupported media type`() = runBlocking {
        val expectedRepo =
            ResultData.Failed(_errorResponse = DummyData.responseUnsupportedMediaType())

        `when`(
            mockStoryPersistence.postStory(
                emptyMap(), null
            )
        ).thenReturn(expectedRepo)
        val actualRepo = storyRepoTest.addStory(StoryBody(description = ""))

        Assert.assertTrue(actualRepo is ResultData.Failed)
        actualRepo as ResultData.Failed
        Assert.assertEquals(
            415,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("Unsupported Media Type", actualRepo.errorResponse.message)
    }

    @Test
    fun `success get stories location`() = runBlocking {
        val responsePersistence = ResultData.Success(DummyData.storiesWithLocation())
        val expectedRepo =
            ResultData.Success(DummyData.storiesWithLocation().asSequence().map { it.asDomain }
                .toList())
        `when`(mockStoryPersistence.storiesWithLocation()).thenReturn(responsePersistence)
        val actual = storyRepoTest.getStoriesLocation()
        Assert.assertTrue(actual is ResultData.Success)
        actual as ResultData.Success
        Assert.assertTrue(actual.data.isNotEmpty())
        Assert.assertEquals(actual.data.size, expectedRepo.data.size)
    }

    @Test
    fun `empty get stories location`() = runBlocking {
        val responsePersistence = ResultData.Success(emptyList<StoryResponse>())

        `when`(mockStoryPersistence.storiesWithLocation()).thenReturn(responsePersistence)
        val actual = storyRepoTest.getStoriesLocation()
        Assert.assertTrue(actual is ResultData.Success)
        actual as ResultData.Success
        Assert.assertTrue(actual.data.isEmpty())
    }
}