package com.f1k375.rycoapp.data.repositories.auth

import com.f1k375.rycoapp.data.models.request.auth.LoginBody
import com.f1k375.rycoapp.data.models.request.auth.RegisterBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.remote.auth.AuthPersistence
import com.f1k375.rycoapp.domain.asDomain
import com.f1k375.rycoapp.utils.DummyData
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthRepositoryTest {

    @Mock
    private lateinit var mockAuthPersistence: AuthPersistence
    private lateinit var autRepoTest: AuthRepositoryImpl

    @Before
    fun setInitSetUp() {
        autRepoTest = AuthRepositoryImpl(mockAuthPersistence)
    }

    @Test
    fun `success login`() = runBlocking {
        val returnPersistence = ResultData.Success(DummyData.loginResponse())
        val expectedRepo = ResultData.Success(DummyData.loginResponse().asDomain)
        val body = LoginBody(
            email = "test@example.com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.login(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.login("test@example.com", "password")

        Assert.assertTrue(actualRepo is ResultData.Success)
        actualRepo as ResultData.Success
        Assert.assertEquals(expectedRepo.data.name, actualRepo.data.name)
    }

    @Test
    fun `failed login user not found`() = runBlocking {

        val returnPersistence = ResultData.Failed(
            _errorResponse = DummyData.userNotFoundResponse()
        )

        val body = LoginBody(
            email = "test@example.com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.login(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.login("test@example.com", "password")
        Assert.assertTrue(actualRepo is ResultData.Failed)

        (actualRepo as ResultData.Failed)
        Assert.assertEquals(
            401,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("User not found", actualRepo.errorResponse.message)
    }

    @Test
    fun `failed login not valid email`() = runBlocking {
        val returnPersistence = ResultData.Failed(
            _errorResponse = DummyData.responseEmailNotValid()
        )

        val body = LoginBody(
            email = "test@com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.login(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.login("test@com", "password")
        Assert.assertTrue(actualRepo is ResultData.Failed)

        (actualRepo as ResultData.Failed)
        Assert.assertEquals(
            400,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("\"email\" must be a valid email", actualRepo.errorResponse.message)
    }

    @Test
    fun `failed login invalid password`() = runBlocking {
        val returnPersistence = ResultData.Failed(
            _errorResponse = DummyData.responsePasswordInvalid()
        )

        val body = LoginBody(
            email = "test@test.com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.login(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.login("test@test.com", "password")
        Assert.assertTrue(actualRepo is ResultData.Failed)

        (actualRepo as ResultData.Failed)
        Assert.assertEquals(
            401,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("Invalid password", actualRepo.errorResponse.message)
    }

    @Test
    fun `success register`() = runBlocking {

        val returnRepo = ResultData.Success(true)
        val body = RegisterBody(
            name = "test",
            email = "test@example.com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.register(body)
        ).thenReturn(returnRepo)
        val actualRepo = autRepoTest.register("test", "test@example.com", "password")
        Assert.assertTrue(actualRepo is ResultData.Success)
    }

    @Test
    fun `failed register email already taken`() = runBlocking {

        val returnPersistence = ResultData.Failed(
            _errorResponse = DummyData.responseEmailAlreadyUsed()
        )

        val body = RegisterBody(
            name = "test",
            email = "test@example.com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.register(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.register("test", "test@example.com", "password")
        Assert.assertTrue(actualRepo is ResultData.Failed)

        (actualRepo as ResultData.Failed)
        Assert.assertEquals(
            400,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("Email is already taken", actualRepo.errorResponse.message)
    }

    @Test
    fun `failed register not valid email`() = runBlocking {
        val returnPersistence = ResultData.Failed(
            _errorResponse = DummyData.responseEmailNotValid()
        )
        val body = RegisterBody(
            name = "test",
            email = "test@com",
            password = "password"
        )
        `when`(
            mockAuthPersistence.register(body)
        ).thenReturn(returnPersistence)
        val actualRepo = autRepoTest.register("test", "test@com", "password")
        Assert.assertTrue(actualRepo is ResultData.Failed)

        (actualRepo as ResultData.Failed)
        Assert.assertEquals(
            400,
            actualRepo.errorResponse.errorCode
        )
        Assert.assertEquals("\"email\" must be a valid email", actualRepo.errorResponse.message)
    }
}