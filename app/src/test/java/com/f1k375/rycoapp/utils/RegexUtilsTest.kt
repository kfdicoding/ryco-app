package com.f1k375.rycoapp.utils

import org.junit.Assert
import org.junit.Test

class RegexUtilsTest {

    @Test
    fun `format email valid`() {
        val email = "test@example.com"
        Assert.assertTrue(RegexUtils.isEmailValid(email))
    }

    @Test
    fun `format email invalid`() {
        val email = "test@example"
        Assert.assertTrue(RegexUtils.isEmailValid(email).not())
    }

    @Test
    fun `valid password contain uppercase, lowercase and length 6`() {
        val password = "1212Ss"
        Assert.assertTrue(RegexUtils.isPasswordValid(password))
    }

    @Test
    fun `invalid password that length lower then 6`() {
        val password = "12Ss"
        Assert.assertTrue(RegexUtils.isPasswordValid(password).not())
    }

    @Test
    fun `invalid password not contain uppercase`() {
        val password = "1212ss"
        Assert.assertTrue(RegexUtils.isPasswordValid(password).not())
    }

    @Test
    fun `invalid password not contain lowercase`() {
        val password = "1212SS"
        Assert.assertTrue(RegexUtils.isPasswordValid(password).not())
    }
}