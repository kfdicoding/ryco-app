package com.f1k375.rycoapp.utils

import com.f1k375.rycoapp.data.models.BaseResponse
import com.f1k375.rycoapp.data.models.payload.auth.UserResponse
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse

object DummyData {

    fun loginResponse(): UserResponse {
        return UserResponse(
            userId = "userId_test",
            name = "Khoirul",
            token = "token_test",
        )
    }

    fun responseEmailNotValid(): BaseResponse<Nothing> {
        return BaseResponse<Nothing>().apply {
            message = "\"email\" must be a valid email"
            errorCode = 400
        }
    }

    fun responsePasswordInvalid(): BaseResponse<Nothing> {
        return BaseResponse<Nothing>().apply {
            message = "Invalid password"
            errorCode = 401
        }
    }

    fun responseEmailAlreadyUsed(): BaseResponse<Nothing> {
        return BaseResponse<Nothing>().apply {
            message = "Email is already taken"
            errorCode = 400
        }
    }

    fun responseUnsupportedMediaType(): BaseResponse<Nothing> {
        return BaseResponse<Nothing>().apply {
            message = "Unsupported Media Type"
            errorCode = 415
        }
    }

    fun userNotFoundResponse(): BaseResponse<Nothing> {
        return BaseResponse<Nothing>().apply {
            message = "User not found"
            errorCode = 401
        }
    }

    fun storiesWithLocation(): List<StoryResponse> {
        val data = ArrayList<StoryResponse>()
        (1..10).forEach {
            data.add(
                StoryResponse(
                    id = "id_$it",
                    name = "name",
                    description = "description",
                    lat = -7.485865,
                    lon = 110.8082397
                )
            )
        }
        return data
    }

    fun storiesNoLocation(): List<StoryResponse> {
        val data = ArrayList<StoryResponse>()
        (1..100).forEach {
            data.add(
                StoryResponse(
                    id = "id_$it",
                    name = "name",
                    description = "description",
                )
            )
        }
        return data
    }

}