package com.f1k375.rycoapp.ui.auth

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.repositories.auth.AuthRepository
import com.f1k375.rycoapp.domain.asDomain
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.sharedpreference.AccountHelper
import com.f1k375.rycoapp.utils.DummyData
import com.f1k375.rycoapp.utils.MainDispatcherRule
import com.f1k375.rycoapp.utils.getOrAwaitValue
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRules = MainDispatcherRule()

    @Mock
    private lateinit var mockAuthRepo: AuthRepository

    @Mock
    private lateinit var mockAccountHelper: AccountHelper
    private lateinit var authViewModelTest: AuthViewModel

    @Before
    fun setInitSetUp() {
        authViewModelTest = AuthViewModel(mockAuthRepo, mockAccountHelper)
    }

    @Test
    fun `success login`(): Unit = runBlocking {
        val expectedData = DummyData.loginResponse().asDomain
        val returnRepo = ResultData.Success(expectedData)

        `when`(
            mockAuthRepo.login("test@example.com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signIn("test@example.com", "password")
        val actualResponse = authViewModelTest.login.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.DataLoaded)
            state as ViewState.DataLoaded
            Assert.assertEquals(state.data.name, expectedData.name)
        }
    }

    @Test
    fun `failed login user not found`(): Unit = runBlocking {

        val returnRepo = ResultData.Failed(
            _errorResponse = DummyData.userNotFoundResponse()
        )

        `when`(
            mockAuthRepo.login("test@example.com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signIn("test@example.com", "password")
        val actualResponse = authViewModelTest.login.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                401,
                state.errorCode
            )
            Assert.assertEquals("Code : 401, User not found", state.message)
        }
    }

    @Test
    fun `failed login not valid email`(): Unit = runBlocking {
        val returnRepo = ResultData.Failed(
            _errorResponse = DummyData.responseEmailNotValid()
        )

        `when`(
            mockAuthRepo.login("test@com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signIn("test@com", "password")
        val actualResponse = authViewModelTest.login.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                400,
                state.errorCode
            )
            Assert.assertEquals("Code : 400, \"email\" must be a valid email", state.message)
        }
    }

    @Test
    fun `failed login invalid password`(): Unit = runBlocking {
        val returnRepo = ResultData.Failed(
            _errorResponse = DummyData.responsePasswordInvalid()
        )
        `when`(
            mockAuthRepo.login("test@test.com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signIn("test@test.com", "password")
        val actualResponse = authViewModelTest.login.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                401,
                state.errorCode
            )
            Assert.assertEquals("Code : 401, Invalid password", state.message)
        }
    }

    @Test
    fun `success register`(): Unit = runBlocking {

        val returnRepo = ResultData.Success(true)

        `when`(
            mockAuthRepo.register("test", "test@example.com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signUp("test", "test@example.com", "password")
        val actualResponse = authViewModelTest.register.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.DataLoaded)
        }
    }

    @Test
    fun `failed register email already taken`(): Unit = runBlocking {
        val returnRepo = ResultData.Failed(
            _errorResponse = DummyData.responseEmailAlreadyUsed()
        )
        `when`(
            mockAuthRepo.register("test", "test@example.com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signUp("test", "test@example.com", "password")
        val actualResponse = authViewModelTest.register.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                400,
                state.errorCode
            )
            Assert.assertEquals("Code : 400, Email is already taken", state.message)
        }
    }

    @Test
    fun `failed register not valid email`(): Unit = runBlocking {
        val returnRepo = ResultData.Failed(
            _errorResponse = DummyData.responseEmailNotValid()
        )

        `when`(
            mockAuthRepo.register("test", "test@com", "password")
        ).thenReturn(returnRepo)
        authViewModelTest.signUp("test", "test@com", "password")
        val actualResponse = authViewModelTest.register.getOrAwaitValue()
        actualResponse.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                400,
                state.errorCode
            )
            Assert.assertEquals("Code : 400, \"email\" must be a valid email", state.message)
        }
    }
}