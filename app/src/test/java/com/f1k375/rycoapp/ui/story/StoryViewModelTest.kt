package com.f1k375.rycoapp.ui.story

import android.content.Context
import android.location.Location
import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.paging.AsyncPagingDataDiffer
import androidx.paging.PagingData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.recyclerview.widget.ListUpdateCallback
import com.f1k375.rycoapp.data.local.story.StoryEntity
import com.f1k375.rycoapp.data.local.story.asEntity
import com.f1k375.rycoapp.data.models.payload.story.StoryResponse
import com.f1k375.rycoapp.data.models.request.story.StoryBody
import com.f1k375.rycoapp.data.remote.ResultData
import com.f1k375.rycoapp.data.repositories.story.StoryRepository
import com.f1k375.rycoapp.domain.StoryDomain
import com.f1k375.rycoapp.domain.asDomain
import com.f1k375.rycoapp.helpers.ViewState
import com.f1k375.rycoapp.helpers.consume
import com.f1k375.rycoapp.helpers.location.GeoLocationDataSource
import com.f1k375.rycoapp.utils.DummyData
import com.f1k375.rycoapp.utils.MainDispatcherRule
import com.f1k375.rycoapp.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import java.lang.ref.WeakReference

@RunWith(MockitoJUnitRunner::class)
class StoryViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRules = MainDispatcherRule()

    @Mock
    private lateinit var mockStoryRepo: StoryRepository

    @Mock
    private lateinit var mockLocationDataSource: GeoLocationDataSource
    private lateinit var storyViewModelTest: StoryViewModel

    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var dummyLocation: Location

    @Before
    fun setInitSetUp() {
        storyViewModelTest = StoryViewModel(mockStoryRepo, mockLocationDataSource)
    }

    @Test
    fun `success add story`(): Unit = runBlocking {
        val body = StoryBody(description = "")
        `when`(mockStoryRepo.addStory(body)).thenReturn(ResultData.Success(true))

        storyViewModelTest.addStory(WeakReference(context), Uri.EMPTY, "")
        val actualPost = storyViewModelTest.addStory.getOrAwaitValue()
        actualPost.consume { state ->
            Assert.assertTrue(state is ViewState.DataLoaded)
        }
    }

    @Test
    fun `failed add story unsupported media type`(): Unit = runBlocking {
        val body = StoryBody(description = "")
        `when`(mockStoryRepo.addStory(body)).thenReturn(ResultData.Failed(DummyData.responseUnsupportedMediaType()))

        storyViewModelTest.addStory(WeakReference(context), Uri.EMPTY, "")
        val actualPost = storyViewModelTest.addStory.getOrAwaitValue()
        actualPost.consume { state ->
            Assert.assertTrue(state is ViewState.Error)
            state as ViewState.Error
            Assert.assertEquals(
                415,
                state.errorCode
            )
            Assert.assertEquals("Code : 415, Unsupported Media Type", state.message)
        }
    }

    @Test
    fun `get stories not empty and return success`() = runBlocking {
        val dummyStory = DummyData.storiesNoLocation()
        val dummyStoryDomain = dummyStory.asSequence().map { it.asDomain }.toList()
        val dataPaging = PagingSourceTestStory.snapshot(dummyStory)

        val expectedStory = MutableStateFlow(dataPaging)

        `when`(mockStoryRepo.getStories("")).thenReturn(expectedStory)
        val actualStory = storyViewModelTest.pagingStory.asLiveData().getOrAwaitValue()

        val differ = AsyncPagingDataDiffer(
            diffCallback = StoriesAdapterPaging.DIFF_CALLBACK,
            updateCallback = noopListUpdateCallback,
            workerDispatcher = Dispatchers.Main
        )
        differ.submitData(actualStory)

        Assert.assertNotNull(differ.snapshot())
        Assert.assertTrue(differ.snapshot().isNotEmpty())
        Assert.assertEquals(dummyStoryDomain.size, differ.snapshot().size)
        Assert.assertEquals(dummyStoryDomain[0].name, differ.snapshot()[0]?.name)
    }

    private val noopListUpdateCallback = object : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) {}
        override fun onRemoved(position: Int, count: Int) {}
        override fun onMoved(fromPosition: Int, toPosition: Int) {}
        override fun onChanged(position: Int, count: Int, payload: Any?) {}
    }

    @Test
    fun `get stories location not empty and return success`(): Unit = runBlocking {
        val dummyStories = DummyData.storiesWithLocation().asSequence().map { it.asDomain }.toList()
        val repoData = ResultData.Success(dummyStories)

        `when`(mockStoryRepo.getStoriesLocation()).thenReturn(repoData)
        storyViewModelTest.getPostStoryLocation()
        val actualStory = storyViewModelTest.storiesLocationObserver.getOrAwaitValue()
        actualStory.consume { state ->

            Assert.assertTrue(state is ViewState.DataLoaded)
            state as ViewState.DataLoaded
            Assert.assertTrue(state.data.isNotEmpty())
            Assert.assertEquals(dummyStories.size, state.data.size)
            Assert.assertEquals(dummyStories[0].name, state.data[0].name)
        }
    }

    @Test
    fun `success get my location`() {
        dummyLocation.latitude = 123.0
        dummyLocation.longitude = -122.0

        val stateFlowLocation = MutableStateFlow(dummyLocation)
        `when`(mockLocationDataSource.locationObservable).thenReturn(stateFlowLocation)
        storyViewModelTest.updateLocation()
        val actualLocation = storyViewModelTest.locationUpdate.getOrAwaitValue()
        actualLocation.consume { state ->
            Assert.assertTrue(state is ViewState.DataLoaded)
            state as ViewState.DataLoaded
            Assert.assertEquals(dummyLocation.latitude, state.data.latitude, 0.0)
            Assert.assertEquals(dummyLocation.longitude, state.data.longitude, 0.0)
        }
    }
}

class PagingSourceTestStory : PagingSource<Int, LiveData<List<StoryDomain>>>() {
    override fun getRefreshKey(state: PagingState<Int, LiveData<List<StoryDomain>>>): Int {
        return 0
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, LiveData<List<StoryDomain>>> {
        return LoadResult.Page(emptyList(), 0, 1)
    }

    companion object {
        fun snapshot(items: List<StoryResponse>): PagingData<StoryEntity> {
            return PagingData.from(items.asSequence().map { it.asEntity }.toList())
        }
    }
}