Repository:
    -Authentication:
        - berhasil melakukan login:
            + Memastikan mengembalikan ResultData.Success
            + Memastikan data user sama seperti yang diharapkan

        - gagal login karena user tidak ditemukan:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 401
            + Memastikan message error nya adalah 'User not found'

        - gagal login karena invalid password:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 401
            + Memastikan message error nya adalah 'Invalid password'

        - gagal login karena invalid email:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah '"email" must be a valid email'
        
        - berhasil melakukan register:
            + Memastikan mengembalikan ResultData.Success

        - gagal melakukan register karena email sudah terdaftar:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah 'Email is already taken'

        - gagal melakukan register karena invalid email:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah '"email" must be a valid email'

    - Story:
        - berhasil membuat story:
            + Memastikan mengembalikan ResultData.Success
            + Memastikan data seperti yang diharapkan

        - gagal membuat story karena format image salah:
            + Memastikan mengembalikan ResultData.Failed
            + Memastikan error code adalah 415
            + Memastikan message error nya adalah 'Unsupported Media Type'
        
        - berhasil memuat data story dengan lokasi:
            + Memastikan mengembalikan ResultData.Success
            + Memastikan data seperti yang diharapkan
        
        - berhasil memuat data story tapi kosong:
            + Memastikan mengembalikan ResultData.Success
            + Memastikan data empty

ViewModel:
    - Authentication:
        - berhasil melakukan login:
            + Memastikan mengembalikan ViewState.DataLoaded
            + Memastikan data user sama seperti yang diharapkan

        - gagal login karena user tidak ditemukan:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 401
            + Memastikan message error nya adalah 'Code : 401, User not found'

        - gagal login karena invalid password:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 401
            + Memastikan message error nya adalah 'Code : 401, Invalid password'

        - gagal login karena invalid email:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah 'Code : 400, "email" must be a valid email'
        
        - berhasil melakukan register:
            + Memastikan mengembalikan ViewState.DataLoaded

        - gagal melakukan register karena email sudah terdaftar:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah 'Code : 400, Email is already taken'

        - gagal melakukan register karena invalid email:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 400
            + Memastikan message error nya adalah 'Code : 400, "email" must be a valid email'

    - Story:
        - berhasil membuat story:
            + Memastikan mengembalikan ViewState.DataLoaded

        - gagal membuat story karena format image salah:
            + Memastikan mengembalikan ViewState.Error
            + Memastikan error code adalah 415
            + Memastikan message error nya adalah 'Code : 415, Unsupported Media Type'

        - berhasil memuat data story dari pagingData:
            + Memastikan data tidak null
            + Memastikan data tidak empty
            + Memastikan jumlah data sesuai dengan yang diharapkan
            + Memsatikan salah satu data sesuai dengan yang diharapkan
        
        - berhasil memuat data story dengan info lokasi:
            + Memastikan mengembalikan ViewState.DataLoaded
            + Memastikan data tidak empty
            + Memastikan jumlah data sesuai dengan yang diharapkan
            + Memsatikan salah satu data sesuai dengan yang diharapkan

        - berhasil memuat data lokasi:
            + Memastikan mengembalikan ViewState.DataLoaded
            + Memastikan data latitude dan longitude sesuai dengan yang diharapkan

RegexUtil:
        - valid email:
            + Memastikan value yang dikembalikan adalah true

        - invalid email:
            + Memastikan value yang dikembalikan adalah false

        - valid password that contain uppercase, lowercase and length 6
            + Memastikan value yang dikembalikan adalah true 
        
        - invalid password that not length < 6:
            + Memastikan value yang dikembalikan adalah false

        - invalid password that not contain uppercase:
            + Memastikan value yang dikembalikan adalah false

        - invalid password that not contain lowecase:
            + Memastikan value yang dikembalikan adalah false


